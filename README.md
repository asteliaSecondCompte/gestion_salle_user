# gestion_salle_user 

- Equipe :
    - Didolla David
    - Dorne Julien  
    - Dziamski Niccolas
	- Garcia Stephane  
	- Massamba Astelia  


## Configuration  

Créer une base de donnée du nom de : jspjsa
Créer un utilisateur ayant pour nom : ndjs
Le mot de passe de cet utilisateur : ndjs 


## Fonctionnalités

### Partie Administrateur

- [ ] Log administrateur

- [ ] Visualiser tout les utilisateurs  
- [ ] Créer un utilisateur
- [ ] Créer un administrateur  
- [ ] Modifier un utilisateur
- [ ] Désactiver un utilisateur 
- [ ] Créer un role 
- [ ] Supprimer un utilisateur

- [ ] Visualiser toutes les salles 
- [ ] Voir une salle en détail 
- [ ] Créer une salle
- [ ] Modifier une salle
- [ ] Ajouter du matériel dans une salle
- [ ] Créer un nouveau type de matériel 
- [ ] Supprimer une salle 
- [ ] Réserver une salle
- [ ] Modifier une réservation
- [ ] Supprimer une réservation

- [ ] Se déconnecter

### Partie Utilisateur

- [ ] Log Utilisateur

- [ ] Visualiser toutes les salles 
- [ ] Voir une salle en détail 

- [ ] Se déconnecter