package fr.afpa.services;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.RolePersonne;
import fr.afpa.interfaces.dto.IDTOGeneral;
import fr.afpa.interfaces.dto.IDTOUtilisateurs;
import fr.afpa.interfaces.services.IServiceUtilisateur;

@Service
public class ServiceUtilisateur implements IServiceUtilisateur {
	
	@Autowired
	private IDTOUtilisateurs dtoUtilisateurs;
	@Autowired
	private IDTOGeneral dtoGeneral;
	
	public Personne utilisateur(String login, String mdp) {
		return dtoUtilisateurs.user(login, mdp);
	}

	@Override
	public boolean ajoutRole(String nom) {
		return dtoUtilisateurs.ajoutRole(nom);
	}

	@Override
	public RolePersonne getRole(String role) {
		Set<RolePersonne> liste = dtoGeneral.getListeRole();
		for (RolePersonne rolePersonne : liste) {
			if(role.equalsIgnoreCase(rolePersonne.getRole()))
				return rolePersonne;
		}
		return null;
	}

	@Override
	public Set<RolePersonne> getAllRole() {
		return dtoGeneral.getListeRole();
	}
}
