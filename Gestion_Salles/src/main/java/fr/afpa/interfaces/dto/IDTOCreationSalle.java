package fr.afpa.interfaces.dto;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Reservation;
import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeSalle;
@Service
public interface IDTOCreationSalle {

	boolean ajoutSalle(Salle salle, String batiment, String type);
	
	boolean ajoutReservation(Reservation reservation, int idSalle);

	List<String> getTypeSalle();

	boolean ajoutSalle(Salle salle, String batiment);

}
