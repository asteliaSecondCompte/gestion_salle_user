package fr.afpa.interfaces.dto;

import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Reservation;

@Service
public interface IDTOVisualisation {

	List<Reservation> listeReservationSalle(int id);
	
	/**
	 * Permet de chercher une reservation via son id
	 * @param id : id de la reservation recherchee
	 * @return la reservation trouvee et null sinon
	 */
	Reservation chercheReservation(int id);
	
}
