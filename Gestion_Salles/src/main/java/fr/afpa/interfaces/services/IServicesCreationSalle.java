package fr.afpa.interfaces.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeSalle;
@Service
public interface IServicesCreationSalle {

	boolean ajoutSalleBdd(Salle salle, String batiment, String type);
	
	boolean creationReservation(String intitule, LocalDate dateDebut, int duree, int idSalle);

	List<String> getTypeSalle();

	boolean ajoutSalleBdd(Salle salle, String batiment);
}
