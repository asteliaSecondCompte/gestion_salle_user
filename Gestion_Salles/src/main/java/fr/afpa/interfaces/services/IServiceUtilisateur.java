package fr.afpa.interfaces.services;

import java.util.Set;

import org.springframework.stereotype.Service;

import fr.afpa.entites.Personne;
import fr.afpa.entites.RolePersonne;

@Service
public interface IServiceUtilisateur {

	public Personne utilisateur(String login, String mdp);

	public boolean ajoutRole(String nom);

	public RolePersonne getRole(String role);

	public Set<RolePersonne> getAllRole();
	
}
