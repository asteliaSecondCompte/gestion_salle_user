package fr.afpa.gestionsalles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.AjoutMateriel;
import fr.afpa.entites.AjoutRole;
import fr.afpa.entites.AjoutSalle;
import fr.afpa.entites.Authentification;
import fr.afpa.entites.Batiment;
import fr.afpa.entites.ChoixSalle;
import fr.afpa.entites.CreateUser;
import fr.afpa.entites.EnvoyerMessage;
import fr.afpa.entites.Message;
import fr.afpa.entites.ModifReserv;
import fr.afpa.entites.ModifSalle;
import fr.afpa.entites.ModifUser;
import fr.afpa.entites.Personne;
import fr.afpa.entites.ReponseAuthentication;
import fr.afpa.entites.Reservation;
import fr.afpa.entites.ReserverInfo;
import fr.afpa.entites.RolePersonne;
import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeSalle;
import fr.afpa.entites.Utilisateur;
import fr.afpa.entites.VoirMessage;
import fr.afpa.entitescom.StatusAjoutModif;
import fr.afpa.entitescom.StatusCreation;
import fr.afpa.entitescom.StatusObjetRetourne;
import fr.afpa.interfaces.controles.IControleAuthentificationUtilisateur;
import fr.afpa.interfaces.controles.IControleChoixUtilisateur;
import fr.afpa.interfaces.controles.IControleCreationUtilisateur;
import fr.afpa.interfaces.controles.IControleGeneral;
import fr.afpa.interfaces.services.IServiceCreation;
import fr.afpa.interfaces.services.IServiceGeneral;
import fr.afpa.interfaces.services.IServiceModification;
import fr.afpa.interfaces.services.IServiceModificationSalle;
import fr.afpa.interfaces.services.IServiceUtilisateur;
import fr.afpa.interfaces.services.IServiceVisualisation;
import fr.afpa.interfaces.services.IServicesCreationSalle;
import fr.afpa.utils.JWTUtils;

/**
 * Handles requests for the application home page.
 */
@CrossOrigin(value = "http://localhost:3000")
@RestController
public class HomeControllerRest {

	@Autowired
	private IServiceGeneral serviceGeneral;
	@Autowired
	private IServiceVisualisation serviceVisualisation;
	@Autowired
	private IServiceModification serviceModification;
	@Autowired
	private IServiceModificationSalle serviceModificationSalle;
	@Autowired
	private IServiceCreation serviceCreation;
	@Autowired
	private IServiceUtilisateur serviceUtilisateur;

	@Autowired
	private IControleAuthentificationUtilisateur controleAuthentificationUtilisateur;
	@Autowired
	private IControleChoixUtilisateur controleChoixUtilisateur;
	@Autowired
	private IControleCreationUtilisateur controleCreationUtilisateur;
	@Autowired
	private IControleGeneral controleGeneral;
	@Autowired
	private IServicesCreationSalle serviceCreationSalle;

	private static final int CODE_ADMIN = 200;
	private static final int CODE_USER = 201;
	private static final int CODE_NOAUTH = 202;

	/**
	 * Controller permettant de logguer la personne en fonction de son type de
	 * profil, admin ou utilisateur
	 * 
	 */
	@PostMapping(value = "/SAPRest")
	@ResponseBody
	public ReponseAuthentication authentificationRest(@RequestBody Authentification authentification) {
		String token = null;
		if (controleAuthentificationUtilisateur.controlePersonneInscrite(authentification.getLogin(),
				authentification.getMdp())) {
			Personne personne = serviceUtilisateur.utilisateur(authentification.getLogin(), authentification.getMdp());
			if (personne instanceof Utilisateur && personne.isActif()) {
				token = JWTUtils.createJWT(authentification.getLogin(), "gestionsalleAPI", "connexion", "utilisateur", 10000000000L);
				return new ReponseAuthentication(0, token);
			} 
			else if (personne instanceof Administrateur) {
				token = JWTUtils.createJWT(authentification.getLogin(), "gestionsalleAPI", "connexion", "administrateur", 10000000000L);
				return new ReponseAuthentication(1, token);
			}
		} 
		return new ReponseAuthentication(-1, token);
	}

	/**
	 * Controller permettant d'afficher l'utilisateur dans la gestion des
	 * utilisateurs
	 * 
	 * @param choix
	 * @return
	 */
	@GetMapping(value = "/SChURest")
	@ResponseBody
	public StatusObjetRetourne choixUserRest(@RequestHeader HttpHeaders header,
			@RequestParam(value = "idUser") String choix) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (controleChoixUtilisateur.verificationChoix(choix)) {
			Map<Integer, Personne> temp = serviceVisualisation.listeTousPersonnes();
			List<Personne> listePersonnes = new ArrayList<Personne>();
			for (Entry<Integer, Personne> personne : temp.entrySet()) {
				if (personne.getValue() instanceof Utilisateur) {
					listePersonnes.add(personne.getValue());
				}
			}
			Optional<Personne> personne = Optional.ofNullable(temp.get(Integer.parseInt(choix)));
			if (personne.isPresent() && personne.get() instanceof Utilisateur && reponse.getCodeRole() == CODE_ADMIN) {
				reponse.setObjetRetourne(personne.get());
				return reponse;
			}
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	/**
	 * Controller permettant de cr�er une personne
	 * 
	 *
	 * @param createUser
	 * @return
	 */
	@PostMapping(value = "/SCURest")
	@ResponseBody
	public StatusCreation createUserRest(@RequestHeader HttpHeaders header, @RequestBody CreateUser createUser) {
		StatusCreation reponse = new StatusCreation();
		reponse.setCodeRole(checkAuthentificationEtRole(header));

		int coderetour = 0;
		RolePersonne roleOk = serviceUtilisateur.getRole("stagiaire");
		LocalDate dateNaissance = LocalDate.now();
		if ("undefined".equals(createUser.getNom()) || "undefined".equals(createUser.getPrenom())
				|| "undefined".equals(createUser.getMail()) || "undefined".equals(createUser.getDateNaissance())
				|| "undefined".equals(createUser.getLogin()) || "undefined".equals(createUser.getPassword())
				|| "undefined".equals(createUser.getPassword2())) {
			reponse.setCodeRetour(-9);
			return reponse;
		}
		if (!controleGeneral.controleNomPrenom(createUser.getNom())) {
			coderetour = -2;
		}
		if (!controleGeneral.controleNomPrenom(createUser.getPrenom())) {
			coderetour = -3;
		}
		if (!createUser.getMail().matches(
				"[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")) {
			coderetour = -4;
		}
		if (controleGeneral.controleRole(createUser.getRole())) {
//			roleOk = serviceCreation.conversionRole(createUser.getRole());
			roleOk = serviceUtilisateur.getRole(createUser.getRole());
		} else {
			coderetour = -5;
		}
		System.out.println(roleOk);
		if (controleGeneral.controleDateDeNaissance(createUser.getDateNaissance())) {
			dateNaissance = serviceGeneral.conversionDate(createUser.getDateNaissance());
		} else {
			coderetour = -6;
		}
		if (!controleCreationUtilisateur.controleLogin(createUser.getLogin())) {
			coderetour = -7;
		}
		if (!createUser.getPassword().equals(createUser.getPassword2())) {
			coderetour = -8;
		}
		if (coderetour == 0) {
			if (reponse.getCodeRole() == CODE_ADMIN && "user".equals(createUser.getCreate())) {
				serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance,
						createUser.getMail(), createUser.getAdresse(), true, roleOk, createUser.getLogin(),
						createUser.getPassword(), false);
			} else if (reponse.getCodeRole() == CODE_ADMIN && "admin".equals(createUser.getCreate())) {
				serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance,
						createUser.getMail(), createUser.getAdresse(), true, roleOk, createUser.getLogin(),
						createUser.getPassword(), true);

			} else if ("pageUser".equals(createUser.getCreate())) {
				serviceCreation.creationPersonne(createUser.getNom(), createUser.getPrenom(), dateNaissance,
						createUser.getMail(), createUser.getAdresse(), false, roleOk, createUser.getLogin(),
						createUser.getPassword(), false);
			}
		}
		reponse.setCodeRetour(coderetour);
		return reponse;
	}

	/**
	 * Controller permettant de modifier un utilisateur (gestion de compte)
	 * 
	 * @param modif
	 * @param password
	 * @param password2
	 * @param nom
	 * @param prenom
	 * @param mail
	 * @param adresse
	 * @param datenaissance
	 * @param id
	 * @return
	 */
	@PutMapping(value = "/SMURest")
	public StatusObjetRetourne modificationUtilisateurRest(@RequestHeader HttpHeaders header, @RequestBody ModifUser modifUser) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN && modifUser.getPassword().equals(modifUser.getPassword2())) {
			Personne user = new Utilisateur();
			user.setNom(modifUser.getNom());
			user.setPrenom(modifUser.getPrenom());
			user.setEmail(modifUser.getMail());
			user.setAdresse(modifUser.getAdresse());
			user.setRole(serviceUtilisateur.getRole(modifUser.getRole()));
			user.setDateNaissance(serviceGeneral.conversionDate(modifUser.getDatenaissance()));
			reponse.setObjetRetourne(serviceModification.modifierUtilisateur(user, Integer.parseInt(modifUser.getId()),
					modifUser.getPassword()));
			return reponse;
		}
		reponse.setObjetRetourne(false);
		return reponse;
	}

	@PutMapping(value = "/SMUActifRest")
	public StatusObjetRetourne activationDesactivationUtilisateurRest(@RequestHeader HttpHeaders header, @RequestParam(value = "idUser") int id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(serviceModification.activerDesactiverUtilisateur(id));
			return reponse;
		}
		reponse.setObjetRetourne(false);
		return reponse;
	}

	@DeleteMapping(value = "/SMURest")
	public StatusObjetRetourne suppressionUtilisateurRest(@RequestHeader HttpHeaders header, @RequestParam(value = "idUser") int id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(serviceModification.supprimerUtilisateur(id));
			return reponse;
		}
		reponse.setObjetRetourne(false);
		return reponse;
	}

	/**
	 * Controlleur permettant de visualiser la liste des utilisateur ( gestion
	 * utilisateur )
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/SVURest")
	@ResponseBody
	public StatusObjetRetourne visualisationUtilisateurRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));

		Map<Integer, Personne> temp = serviceVisualisation.listeTousPersonnes();
		List<Personne> listePersonnes = new ArrayList<Personne>();
		for (Entry<Integer, Personne> personne : temp.entrySet()) {
			listePersonnes.add(personne.getValue());
		}
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(listePersonnes);
		}
		return reponse;
	}

	/**
	 * Controlleur permettant de visualiser la liste des utilisateurs sans les
	 * administrateurs ( gestion utilisateur )
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/SVUUserRest")
	@ResponseBody
	public StatusObjetRetourne visualisationUtilisateurUniquementRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		Map<Integer, Personne> temp = serviceVisualisation.listeTousPersonnes();
		List<Personne> listePersonnes = new ArrayList<Personne>();
		for (Entry<Integer, Personne> personne : temp.entrySet()) {
			if (personne.getValue() instanceof Utilisateur) {
				listePersonnes.add(personne.getValue());
			}
		}
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(listePersonnes);
		}
		return reponse;
	}

	/**
	 * Controller permettant d'envoyer un message
	 * 
	 * @param destinataire
	 * @param objet
	 * @param contenu
	 * @return un boolean pour savoir si le destinaire et valide ou pas
	 */
	@PostMapping(value = "/EMRest")
	@ResponseBody
	public boolean envoyerMessageRest(@RequestBody EnvoyerMessage envoyerMessage) {
		if (controleAuthentificationUtilisateur.controleDestinataire(envoyerMessage.getDestinataire())) {
			return serviceCreation.creationMessage(envoyerMessage.getExpediteur(), envoyerMessage.getDestinataire(),
					envoyerMessage.getObjet(), envoyerMessage.getContenu(), LocalDateTime.now());
		} else {
			return false;
		}
	}

	/**
	 * Controller permettant d'achiver un message
	 * 
	 * @param id r�cup�ration de l'id pour mettre � true l'archivage du message dans
	 *           la base de donn�e
	 * @return redirection vers boite de r�ception
	 */
	@PutMapping(value = "/ARCRest")
	@ResponseBody
	public boolean archivageRest(@RequestBody Integer id) {
		return serviceModification.archiverMsg(id);
	}

	/**
	 * Controller permettant d'afficher la liste des messages archiver dans la page
	 * Messages archiv�s
	 * 
	 * @return le model contenant la liste des messages archiv�s et la redirection
	 */
	@GetMapping(value = "/MARest")
	@ResponseBody
	public List<Message> boiteArchiveRest(@RequestBody String expediteur) {
		List<Message> lm = serviceVisualisation.afficherListeMessage(expediteur);
		lm.addAll(serviceVisualisation.afficherListeMessageEnvoyer(expediteur));
		return lm;
	}

	/**
	 * Controller permettant d'afficher la liste des messages dans la boite de
	 * r�ception
	 * 
	 * @return les donn�es du model et la vue
	 */
	@GetMapping(value = "/BRRest")
	@ResponseBody
	public List<Message> boiteReceptionRest(@RequestBody String expediteur) {
		return serviceVisualisation.afficherListeMessage(expediteur);
	}

	/**
	 * Controller qui permet d'afficher la liste des messages envoy�s
	 * 
	 * @return les donn�es du model et la redirection
	 */
	@GetMapping(value = "/MERest")
	@ResponseBody
	public List<Message> messageEnvoyeRest(@RequestBody String expediteur) {
		return serviceVisualisation.afficherListeMessageEnvoyer(expediteur);
	}

	/**
	 * Controller permettant de visualiser un message envoy�
	 * 
	 * @param destinataire
	 * @param objet
	 * @param contenu
	 * @param date
	 * @return un model contenant le message et la redirection
	 */
	@GetMapping(value = "/voirERest")
	@ResponseBody
	public VoirMessage voirMessageRest(@RequestBody VoirMessage voirMessage) {
		return voirMessage;
	}

	@GetMapping(value = "/SChSRest")
	@ResponseBody
	public StatusObjetRetourne choixSalleRest(@RequestHeader HttpHeaders header, @RequestParam(value = "idSalle") String choix) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			reponse.setObjetRetourne(serviceModificationSalle.getSalle(choix));
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@PutMapping(value = "/MSRest")
	public StatusObjetRetourne modificationSalleRest(@RequestHeader HttpHeaders header, @RequestBody ModifSalle modifSalle) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			Salle salle = new Salle();
			salle.setId(Integer.parseInt(modifSalle.getId()));
			if (!"".equals(modifSalle.getNomsalle()))
				salle.setNom(modifSalle.getNomsalle());
			if (!"".equals(modifSalle.getCapacite()))
				salle.setCapacite(Integer.parseInt(modifSalle.getCapacite()));
			if (!"".equals(modifSalle.getSurface()))
				salle.setSurface(Float.parseFloat(modifSalle.getSurface()));
			if (!"".equals(modifSalle.getNumsalle()))
				salle.setNumero(modifSalle.getNumsalle());
			salle.setListeMateriels(modifSalle.getListeMateriels());
			reponse.setObjetRetourne(serviceModificationSalle.updateSalle(salle, modifSalle.getType(), modifSalle.getBatiment()));
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
		
	}

	@DeleteMapping(value = "/MSRest")
	public StatusObjetRetourne suppressionSalleRest(@RequestHeader HttpHeaders header, @RequestParam(value = "idSalle") int id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(serviceModificationSalle.supprimerSalle(id));
			return reponse;
		}
		reponse.setObjetRetourne(false);
		return reponse;
	}

	/**
	 * Return la vue choix de la salle
	 */
	@GetMapping(value = "/csRest")
	@ResponseBody
	public StatusObjetRetourne choixSalleRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			reponse.setObjetRetourne(serviceModificationSalle.voirSalle());
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/vsRest")
	@ResponseBody
	public StatusObjetRetourne voirSalleRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			reponse.setObjetRetourne(serviceModificationSalle.listeSalleComplete());
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/vrcRest")
	@ResponseBody
	public StatusObjetRetourne voirSalleCompleteRest(@RequestHeader HttpHeaders header,
			@RequestParam(value = "idSalle") String id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH && controleChoixUtilisateur.verificationChoix(id)) {
			reponse.setObjetRetourne(serviceModificationSalle.getSalleComplete(id));
		}
		return reponse;

	}

	@GetMapping(value = "/scRest")
	@ResponseBody
	public StatusObjetRetourne salleChoisiRest(@RequestHeader HttpHeaders header, @RequestBody ChoixSalle choixSalle) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			Map<String, Object> mv = new HashMap<String, Object>();
			mv.put("id", choixSalle.getId());
			Salle salle = serviceModificationSalle.getSalle(choixSalle.getId());
			if (salle != null) {
				if ("Reserver".equals(choixSalle.getRes())) {
					mv.put("reservations", serviceVisualisation.listeReservations(Integer.parseInt(choixSalle.getId())));
				} else {
					mv.put("materiel", serviceModificationSalle.voirMateriel(Integer.parseInt(choixSalle.getId())));
					mv.put("salle", salle);
				}
			} else {
				mv.put("allroom", serviceModificationSalle.voirSalle());
			}
			reponse.setObjetRetourne(mv);
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/crsRest")
	@ResponseBody
	public StatusObjetRetourne createSalleRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			Map<String, Object> res = new HashMap<String, Object>();
			res.put("listebatiment", serviceModificationSalle.listerBatiment());
			res.put("listeTypeSalle", TypeSalle.values());
			reponse.setObjetRetourne(res);
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/crbRest")
	@ResponseBody
	public StatusObjetRetourne afficheBatimentRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(serviceModificationSalle.listerBatiment());
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/rtsRest")
	@ResponseBody
	public StatusObjetRetourne recupTypeSalleRest(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			reponse.setObjetRetourne(serviceCreationSalle.getTypeSalle());
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@PutMapping(value = "/rmsRest")
	@ResponseBody
	public boolean redirectionModifSalleRest(@RequestBody ModifSalle modifSalle) {
		switch (modifSalle.getModif()) {
		case "valider":
			Salle salle = new Salle(modifSalle.getNumsalle(), modifSalle.getNomsalle(),
					Integer.parseInt(modifSalle.getCapacite()), Float.parseFloat(modifSalle.getSurface()),
					TypeSalle.valueOf(modifSalle.getType().toUpperCase()));
			salle.setId(Integer.parseInt(modifSalle.getId()));
			return serviceModificationSalle.updateSalle(salle);
		case "supprimer":
			return serviceModificationSalle.supprimerSalle(Integer.parseInt(modifSalle.getId()));
		default:
			break;
		}
		return false;
	}

	/**
	 * @param batiment
	 * @param numero   de salle
	 * @param nom      de salle
	 * @param surface  de la salle
	 * @param capacite de la salle
	 * @param type     de salle Return la vue modification de la salle
	 */
	@PostMapping(value = "/asbddRest")
	@ResponseBody
	public StatusObjetRetourne ajoutSalleBddRest(@RequestHeader HttpHeaders header,
			@RequestBody AjoutSalle ajoutSalle) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN) {
			if ("".equals(ajoutSalle.getNumsalle()) || "".equals(ajoutSalle.getNomsalle())) {
				reponse.setObjetRetourne(false);
				return reponse;
			}
			Salle salle = new Salle(ajoutSalle.getNumsalle(), ajoutSalle.getNomsalle(),
					Integer.parseInt(ajoutSalle.getCapacite()),ajoutSalle.getImage(), Float.parseFloat(ajoutSalle.getSurface()),
					TypeSalle.values()[Integer.parseInt(ajoutSalle.getType()) - 2]);
			reponse.setObjetRetourne(serviceCreationSalle.ajoutSalleBdd(salle, ajoutSalle.getBatiment()));
		}
		return reponse;
	}

	@PostMapping(value = "/ReserverRest")
	@ResponseBody
	public StatusObjetRetourne reserverRest(@RequestHeader HttpHeaders header, @RequestBody ReserverInfo reserverInfo) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN && controleGeneral.controleDateDeNaissance(reserverInfo.getDebut())
				&& controleGeneral.controleTailleObjetMesage(reserverInfo.getIntitule())
				&& controleChoixUtilisateur.verificationChoix(reserverInfo.getDuree())
				&& controleGeneral.controleDatePasObsolete(serviceGeneral.conversionDate(reserverInfo.getDebut()))
				&& controleGeneral.controleCollisionDates(
						serviceVisualisation.listeReservations(Integer.parseInt(reserverInfo.getIdSalle())),
						serviceGeneral.conversionDate(reserverInfo.getDebut()),
						Integer.parseInt(reserverInfo.getDuree()))) {
			reponse.setObjetRetourne(serviceCreationSalle.creationReservation(reserverInfo.getIntitule(),
					serviceGeneral.conversionDate(reserverInfo.getDebut()), Integer.parseInt(reserverInfo.getDuree()),
					Integer.parseInt(reserverInfo.getIdSalle())));
			return reponse;
		}
		reponse.setObjetRetourne(false);
		return reponse;
	}

	/**
	 * Permet d'annuler une reservation si celle-ci n'est pas en cours ou n'est pas
	 * encore passee
	 * 
	 * @param idReservation : id de la reservation a annuler
	 * @return true si la reservation a bien ete annulee et false sinon
	 */
	@DeleteMapping(value = "/AnnulerRest")
	@ResponseBody
	public StatusObjetRetourne supprimerReservation(@RequestHeader HttpHeaders header, @RequestParam(value = "idReservation") int idReservation) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		Reservation reserv = serviceVisualisation.chercheReservation(idReservation);
		if (reponse.getCodeRole() == CODE_ADMIN && reserv != null && controleGeneral.controleReservationEnCours(reserv)) {
			reponse.setObjetRetourne(serviceModificationSalle.annulerReservation(idReservation));
			return reponse;
		} else {
			reponse.setObjetRetourne(false);
			return reponse;
		}
	}

	/**
	 * Permet de modifier une resevation
	 * 
	 * @param modifReserv
	 * @return
	 */
	@PutMapping(value = "/ModifReservRest")
	@ResponseBody
	public StatusObjetRetourne modifierReservation(@RequestHeader HttpHeaders header, @RequestBody ModifReserv modifReserv) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		Reservation reserv = serviceVisualisation.chercheReservation(modifReserv.getIdReservation());
		Salle salle = serviceModificationSalle.getSalle(modifReserv.getIdSalle());
		List<Reservation> listeReservations = serviceVisualisation
				.listeReservations(Integer.parseInt(modifReserv.getIdSalle()));
		if (reserv != null) {
			listeReservations.remove(reserv);
		}
		if (reserv != null && salle != null && controleGeneral.controleReservationEnCours(reserv)
				&& controleGeneral.controleDateDeNaissance(modifReserv.getDateDebut())
				&& controleGeneral.controleTailleObjetMesage(modifReserv.getIntitule())
				&& controleChoixUtilisateur.verificationChoix(modifReserv.getDuree())
				&& controleGeneral.controleDatePasObsolete(serviceGeneral.conversionDate(modifReserv.getDateDebut()))
				&& controleGeneral.controleCollisionDates(listeReservations,
						serviceGeneral.conversionDate(modifReserv.getDateDebut()),
						Integer.parseInt(modifReserv.getDuree()))) {
			Reservation reservation = new Reservation(modifReserv.getIntitule(),
					serviceGeneral.conversionDate(modifReserv.getDateDebut()),
					serviceGeneral.conversionDate(modifReserv.getDateDebut())
							.plusDays(Integer.parseInt(modifReserv.getDuree())));
			reservation.setId(modifReserv.getIdReservation());
			reponse.setObjetRetourne(serviceModificationSalle.modifierReservation(reservation,
					Integer.parseInt(modifReserv.getIdSalle())));
			return reponse;
		} else {
			reponse.setObjetRetourne(false);
			return reponse;
		}
	}

	/**
	 * Permet de retourner la liste de tous les roles
	 * 
	 * @return la liste de tous les roles
	 */
	@GetMapping(value = "/ListeRolesRest")
	@ResponseBody
	public StatusObjetRetourne listeRoles(@RequestHeader HttpHeaders header) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			reponse.setObjetRetourne(serviceUtilisateur.getAllRole());
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	/**
	 * Permert de retourner la liste des reservations d'une salle
	 * 
	 * @param id : id de la salle
	 * @return la liste des reservations de la salle dont l'id est en parametre
	 */
	@GetMapping(value = "/ListeReservsRest")
	@ResponseBody
	public StatusObjetRetourne listeReservations(@RequestHeader HttpHeaders header, @RequestParam(value = "idSalle") String id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH && controleChoixUtilisateur.verificationChoix(id)) {
			reponse.setObjetRetourne(serviceVisualisation.listeReservations(Integer.parseInt(id)));
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/choixReserv")
	@ResponseBody
	public StatusObjetRetourne choixReservation(@RequestHeader HttpHeaders header, @RequestParam(value = "idReserv") String choix) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() == CODE_ADMIN && controleChoixUtilisateur.verificationChoix(choix)) {
			reponse.setObjetRetourne(serviceVisualisation.chercheReservation(Integer.parseInt(choix)));
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@GetMapping(value = "/ListeMaterielsRest")
	@ResponseBody
	public StatusObjetRetourne listeMateriels(@RequestHeader HttpHeaders header, @PathVariable(value = "idSalle") int id) {
		StatusObjetRetourne reponse = new StatusObjetRetourne();
		reponse.setCodeRole(checkAuthentificationEtRole(header));
		if (reponse.getCodeRole() != CODE_NOAUTH) {
			reponse.setObjetRetourne(serviceModificationSalle.getMateriel(id));
			return reponse;
		}
		reponse.setObjetRetourne(null);
		return reponse;
	}

	@PostMapping(value = "/AMRest")
	@ResponseBody
	public StatusAjoutModif ajoutMaterielRest(@RequestHeader HttpHeaders header,
			@RequestBody AjoutMateriel ajoutMateriel) {
		int auth = checkAuthentificationEtRole(header);
		StatusAjoutModif reponse = new StatusAjoutModif(CODE_NOAUTH, false);
		if (auth == CODE_ADMIN) {
			reponse.setCodeRole(CODE_ADMIN);
			reponse.setFait(serviceModificationSalle.ajoutMateriel(ajoutMateriel.getNom()));
		} else if (auth == CODE_USER) {
			reponse.setCodeRole(CODE_USER);
			reponse.setFait(false);
		}
		return reponse;
	}
	
	@PostMapping(value = "/ARRest")
	@ResponseBody
	public StatusAjoutModif ajoutRoleRest(@RequestHeader HttpHeaders header,
			@RequestBody AjoutRole ajoutRole) {
		int auth = checkAuthentificationEtRole(header);
		StatusAjoutModif reponse = new StatusAjoutModif(CODE_NOAUTH, false);
		if (auth == CODE_ADMIN) {
			reponse.setCodeRole(CODE_ADMIN);
			reponse.setFait(serviceUtilisateur.ajoutRole(ajoutRole.getNom()));
		} else if (auth == CODE_USER) {
			reponse.setCodeRole(CODE_USER);
			reponse.setFait(false);
		}
		return reponse;
	}

	@GetMapping(value = "/verifAutorisationRest")
	@ResponseBody
	public int verifAutorisation(@RequestHeader HttpHeaders header) {
		return checkAuthentificationEtRole(header);
	}
	
	/**
	 * 
	 * @param header
	 * @return CODE_ADMIN si la personne authentifiee est un admin, CODE_USER si la
	 *         personne authentifiee est un utilisateur et CODE_NOAUTH si elle n'est
	 *         pas authentifiee
	 */
	public int checkAuthentificationEtRole(HttpHeaders header) {
		// Permet de recuperer dans le httpheader la valeur du champ Authorization
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JWTUtils.isAuthentifie(token.get(), "administrateur")) {
				return CODE_ADMIN;
			} else if (token.isPresent() && JWTUtils.isAuthentifie(token.get(), "utilisateur")) {
				return CODE_USER;
			}
		}
		return CODE_NOAUTH;
	}
}
