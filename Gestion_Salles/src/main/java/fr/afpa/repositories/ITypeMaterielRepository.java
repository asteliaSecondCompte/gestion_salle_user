package fr.afpa.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.entitespersistees.MaterielBDD;
import fr.afpa.entitespersistees.TypeMaterielBDD;

@Repository
public interface ITypeMaterielRepository extends JpaRepository<TypeMaterielBDD, Integer> {

	TypeMaterielBDD findByMateriel(MaterielBDD materiel);

	
}
