package fr.afpa.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString
public /*enum*/class RolePersonne {

	/*FORMATEUR(1, "Formateur"), STAGIAIRE(2, "Stagiaire");

	private int id;
	private String role;

	RolePersonne(int id, String role) {
		this.id = id;
		this.role = role;
	}
	*/
	
	private int id;
	private String role;
	
}
