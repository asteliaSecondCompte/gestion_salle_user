package fr.afpa.entites;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString
public /*enum*/class TypeMateriel {
	
	private Integer id;
	private String type;
//	ORDINATEUR("ordinateur"), PRISE_RESEAUX("prise reseaux"), RETROPROJECTEUR("retroprojecteur");
//	private String type;
//
//	TypeMateriel(String type) {
//		this.type = type;
//	}
}
