package fr.afpa.entites;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class VoirMessage {

	private String expediteur;
	private List<String> destinataires;
	private String objet;
	private String contenu;
	private String date;
	
}
