package fr.afpa.entites;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ModifSalle {

	private String numsalle;
	private String nomsalle;
	private String surface;
	private String capacite;
	private String type;
	private String modif;
	private String id;
	private String batiment;
	private List<Materiel> listeMateriels;
	
}
