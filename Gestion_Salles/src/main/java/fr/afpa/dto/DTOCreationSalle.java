package fr.afpa.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entites.Reservation;
import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeSalle;
import fr.afpa.entitespersistees.MaterielBDD;
import fr.afpa.entitespersistees.ReservationBDD;
import fr.afpa.entitespersistees.SalleBDD;
import fr.afpa.entitespersistees.TypeMaterielBDD;
import fr.afpa.entitespersistees.TypeSalleBDD;
import fr.afpa.interfaces.dto.IDTOCreationSalle;
import fr.afpa.interfaces.dto.IDTOGeneral;
import fr.afpa.repositories.IBatimentRepository;
import fr.afpa.repositories.ICentreRepository;
import fr.afpa.repositories.IMaterielRepository;
import fr.afpa.repositories.IReservationRepository;
import fr.afpa.repositories.ISalleRepository;
import fr.afpa.repositories.ITypeMaterielRepository;
import fr.afpa.repositories.ITypeSalleRepository;

@Service

public class DTOCreationSalle implements IDTOCreationSalle {

	@Autowired
	private ITypeSalleRepository typeSalleRepository;

	@Autowired
	private ISalleRepository salleRepository;

	@Autowired
	private ICentreRepository centreRepository;

	@Autowired
	private IBatimentRepository batimentRepository;

	@Autowired
	private IReservationRepository reservationRepository;

	@Autowired
	private IDTOGeneral dtoGeneral;

	@Autowired
	private ITypeMaterielRepository typeMaterielRepository;
	
	@Autowired
	private IMaterielRepository materielRepository;

	@Override
	public boolean ajoutSalle(Salle salle, String batiment, String type) {
		SalleBDD salleBDD = new SalleBDD();

		salleBDD.setBatiment(batimentRepository.findById(Integer.parseInt(batiment)).get());
		salleBDD.setCapacite(salle.getCapacite());
		salleBDD.setId(salle.getId());
		salleBDD.setNom(salle.getNom());
		salleBDD.setNumero(salle.getNumero());
		salleBDD.setImage(salle.getImage());
		salleBDD.setSurface(salle.getSurface());
		salleBDD.setTypeSalle(typeSalleRepository.findById(Integer.parseInt(batiment)).get());
		salleRepository.save(salleBDD);

		return true;
	}

	@Override
	public boolean ajoutReservation(Reservation reservation, int idSalle) {
		try {
			ReservationBDD reservBDD = dtoGeneral.reservationToReservationBDD(reservation, idSalle);
			reservationRepository.save(reservBDD);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<String> getTypeSalle() {
		List<TypeSalleBDD> listeBDD = typeSalleRepository.findAll();
		List<String> liste = new ArrayList<String>();
		for (TypeSalleBDD typeSalleBDD : listeBDD) {
			liste.add(typeSalleBDD.getType());
		}
		return liste;
	}

	@Override
	public boolean ajoutSalle(Salle salle, String batiment) {
		List<TypeMaterielBDD> listeMaterielBDD = typeMaterielRepository.findAll();
		
		SalleBDD salleBDD = new SalleBDD();
		salleBDD.setMateriel(new ArrayList<MaterielBDD>());
		
		salleBDD.setBatiment(batimentRepository.findById(Integer.parseInt(batiment)).get());
		salleBDD.setCapacite(salle.getCapacite());
		salleBDD.setId(salle.getId());
		salleBDD.setNom(salle.getNom());
		salleBDD.setNumero(salle.getNumero());
		salleBDD.setImage(salle.getImage());
		salleBDD.setSurface(salle.getSurface());
		salleBDD.setTypeSalle(typeSalleRepository.findById(salle.getTypeSalle().getId()).get());
		salleRepository.save(salleBDD);
		for (TypeMaterielBDD typeMaterielBDD : listeMaterielBDD) {
			MaterielBDD materielBDD = new MaterielBDD();
			materielBDD.setTypeMateriel(typeMaterielBDD);
			salleBDD.getMateriel().add(materielBDD);
			materielBDD.setSalle(salleBDD);
			materielRepository.save(materielBDD);
		}

		return true;
	}

}
