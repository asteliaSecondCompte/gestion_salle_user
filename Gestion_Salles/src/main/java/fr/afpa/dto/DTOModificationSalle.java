package fr.afpa.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.entites.Batiment;
import fr.afpa.entites.Materiel;
import fr.afpa.entites.Reservation;
import fr.afpa.entites.Salle;
import fr.afpa.entites.TypeMateriel;
import fr.afpa.entitespersistees.BatimentBDD;
import fr.afpa.entitespersistees.MaterielBDD;
import fr.afpa.entitespersistees.ReservationBDD;
import fr.afpa.entitespersistees.SalleBDD;
import fr.afpa.entitespersistees.TypeMaterielBDD;
import fr.afpa.entitespersistees.TypeSalleBDD;
import fr.afpa.interfaces.dto.IDTOGeneral;
import fr.afpa.interfaces.dto.IDTOModificationSalle;
import fr.afpa.repositories.IBatimentRepository;
import fr.afpa.repositories.IMaterielRepository;
import fr.afpa.repositories.IReservationRepository;
import fr.afpa.repositories.ISalleRepository;
import fr.afpa.repositories.ITypeMaterielRepository;
import fr.afpa.repositories.ITypeSalleRepository;

@Service
public class DTOModificationSalle implements IDTOModificationSalle {
	@Autowired
	private ISalleRepository modificationSalleRepository;
	@Autowired
	private IDTOGeneral dtoGeneral;
	@Autowired
	private IBatimentRepository dtoBatiment;
	@Autowired
	private ITypeMaterielRepository typeMaterielRepository;
	@Autowired
	private IMaterielRepository materielRepository;
	@Autowired
	private IReservationRepository reservationRepository;
	@Autowired
	private ITypeSalleRepository typeSalleRepository;

	@Override
	public List<Salle> listeSalles() {
		List<Salle> listeSalle = new ArrayList<Salle>();
		List<SalleBDD> listeSalles = modificationSalleRepository.findAll();
		for (SalleBDD salleBDD : listeSalles) {
			listeSalle.add(dtoGeneral.salleBDDToSalle(salleBDD));
		}
		return listeSalle;
	}

	@Override
	public List<Salle> voirSalles() {
		List<Salle> listeSalle = new ArrayList<Salle>();
		List<SalleBDD> listeSalles = modificationSalleRepository.findAll();
		for (SalleBDD salleBDD : listeSalles) {
			listeSalle.add(dtoGeneral.salleBDDToSalleComplete(salleBDD));
		}
		return listeSalle;
	}

	@Override
	public Salle choixSalle(String id) {
		if (modificationSalleRepository.existsById(Integer.parseInt(id)))
			return dtoGeneral.salleBDDToSalle(modificationSalleRepository.findById(Integer.parseInt(id)).get());
		else
			return null;
	}

	@Override
	public Salle choixSalleComplete(String id) {
		if (modificationSalleRepository.existsById(Integer.parseInt(id))) {
			SalleBDD salleBDD = modificationSalleRepository.findById(Integer.parseInt(id)).get();
			salleBDD.setReservation(reservationRepository.findBySalle(salleBDD));
			salleBDD.setMateriel(materielRepository.findBySalle(salleBDD));
			return dtoGeneral.salleBDDToSalleComplete(salleBDD);
		} else {
			return null;
		}
	}

	@Override
	public ArrayList<Batiment> listerBatiment() {
		ArrayList<Batiment> bat = new ArrayList<Batiment>();
		List<BatimentBDD> batBdd = dtoBatiment.findAll();
		for (BatimentBDD batimentBDD : batBdd) {
			bat.add(dtoGeneral.batimentBDDToBatiment(batimentBDD));
		}
		return bat;
	}

	@Override
	public boolean updateSalle(Salle salle) {
		if (modificationSalleRepository.findById(salle.getId()).isPresent()) {
			SalleBDD salleBDD = modificationSalleRepository.findById(salle.getId()).get();
			if (salle.getCapacite() != 0)
				salleBDD.setCapacite(salle.getCapacite());
			if (salle.getNom() != null)
				salleBDD.setNom(salle.getNom());
			if (salle.getNumero() != null)
				salleBDD.setNumero(salle.getNumero());
			if (salle.getSurface() != 0)
				salleBDD.setSurface(salle.getSurface());
			modificationSalleRepository.save(salleBDD);
			List<MaterielBDD> materiel = materielRepository.findBySalleOrderByTypemateriel(salleBDD.getId());
			for (MaterielBDD materielBDD : materiel) {
				for (Materiel mat : salle.getListeMateriels()) {
					if (materielBDD.getId() == mat.getId()) {
						materielBDD.setQuantite(mat.getQuantite());
						materielRepository.save(materielBDD);
					}
				}
			}
			if (salle.getTypeSalle() != null)
				salleBDD.setTypeSalle(typeSalleRepository.findByType(salle.getTypeSalle().getType()));
			return true;
		}
		return false;
	}

	@Override
	public boolean updateSalle(Salle salle, String idTypeSalle, String idBatiment) {
		if (modificationSalleRepository.findById(salle.getId()).isPresent()) {
			SalleBDD salleBDD = modificationSalleRepository.findById(salle.getId()).get();
			if (salle.getCapacite() != 0)
				salleBDD.setCapacite(salle.getCapacite());
			if (salle.getNom() != null)
				salleBDD.setNom(salle.getNom());
			if (salle.getNumero() != null)
				salleBDD.setNumero(salle.getNumero());
			if (salle.getSurface() != 0)
				salleBDD.setSurface(salle.getSurface());
			List<MaterielBDD> materiel = materielRepository.findBySalleOrderByTypemateriel(salleBDD.getId());
			for (MaterielBDD materielBDD : materiel) {
				for (Materiel mat : salle.getListeMateriels()) {
					if (materielBDD.getId().equals(mat.getId())) {
						materielBDD.setQuantite(mat.getQuantite());
						materielRepository.save(materielBDD);
					}
				}
			}
			if (idTypeSalle != null && !"0".equals(idTypeSalle))
				salleBDD.setTypeSalle(typeSalleRepository.findById(Integer.parseInt(idTypeSalle)).get());
			if (idBatiment != null && !"0".equals(idBatiment))
				salleBDD.setBatiment(dtoBatiment.findById(Integer.parseInt(idBatiment)).get());
			modificationSalleRepository.save(salleBDD);
			return true;
		}
		return false;
	}

	@Override
	public boolean supprimerSalle(int parseInt) {
		modificationSalleRepository.deleteById(parseInt);
		return true;
	}

	@Override
	public Map<String, Integer> voirMateriel(int id) {
//		Map<String, Integer> materiels = new HashMap<String, Integer>();
//		TypeMateriel[] type = TypeMateriel.values();
//		for (TypeMateriel typeMateriel : type) {
//			List<Integer> i = materielRepository.findQuantiteByTypeMateriel(id, typeMateriel.getType().toUpperCase());
//			materiels.put(typeMateriel.getType().toUpperCase(), (!i.isEmpty()) ? i.get(0) : 0);
//		}
//		return materiels;
		Map<String, Integer> materiels = new HashMap<String, Integer>();
		Set<TypeMateriel> listeMateriel = dtoGeneral.getListeMateriel();
		for (TypeMateriel typeMateriel : listeMateriel) {
			List<Integer> ids = materielRepository.findQuantiteByTypeMateriel(id, typeMateriel.getType().toUpperCase());
			materiels.put(typeMateriel.getType().toUpperCase(), (!ids.isEmpty()) ? ids.get(0) : 0);
		}
		return materiels;
	}

	@Override
	public boolean supprimerReservation(int id) {
		try {
			reservationRepository.deleteById(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean modifierReservation(Reservation reserv, int idSalle) {
		try {
			if (reservationRepository.findById(reserv.getId()).isPresent()) {
				ReservationBDD reservBDD = dtoGeneral.reservationToReservationBDD(reserv, idSalle);
				reservBDD.setId(reserv.getId());
				reservationRepository.save(reservBDD);
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean ajoutMateriel(String nom) {
		if (nom == null)
			return false;
		Set<TypeMateriel> listeMateriel = dtoGeneral.getListeMateriel();
		for (TypeMateriel typeMateriel : listeMateriel) {
			if (typeMateriel.getType().equalsIgnoreCase(nom)) {
				return false;
			}
		}
		TypeMaterielBDD typeMaterielBDD = new TypeMaterielBDD();
		typeMaterielBDD.setType(nom.toUpperCase());
		typeMaterielRepository.save(typeMaterielBDD);
		miseAJourSalle();
		return true;
	}

	private boolean miseAJourSalle() {
		List<SalleBDD> listeSalle = modificationSalleRepository.findAll();
		List<TypeMaterielBDD> listeTypeMateriel = typeMaterielRepository.findAll();
		for (SalleBDD salleBDD : listeSalle) {
			Salle salle = dtoGeneral.salleBDDToSalleComplete(salleBDD);
			System.out.println(salle);
			List<String> list = new ArrayList<String>();
			if (salle.getListeMateriels() != null) {
				for (Materiel materiel : salle.getListeMateriels()) {
					list.add(materiel.getType().getType());
				}
				for (TypeMaterielBDD typeMaterielBDD : listeTypeMateriel) {
					if (!list.contains(typeMaterielBDD.getType())) {
						MaterielBDD mat = new MaterielBDD();
						mat.setTypeMateriel(typeMaterielBDD);
						mat.setSalle(salleBDD);
						mat.setQuantite(0);
						materielRepository.save(mat);
					}
				}
			}
		}
		return true;
	}

}
