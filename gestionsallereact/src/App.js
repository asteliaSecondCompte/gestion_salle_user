import React from 'react';
import './App.css';
import Chemin from './Chemin/Chemin';
import './Pages/Page.css'
import { ToastProvider } from 'react-toast-notifications'

function App() {
  return (
    <ToastProvider>
      <div className="App">
        <Chemin />
      </div>
    </ToastProvider>
  );
}

export default App;
