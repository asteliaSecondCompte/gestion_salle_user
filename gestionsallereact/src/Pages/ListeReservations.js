import React from 'react';
import { Container } from 'react-bootstrap';
import RecupReservations from '../Composants/RecupReservations';

export default function ListeReservations() {
    let idSalle = parseInt(document.URL.split("=")[1]);
    return (
        
        <Container className="texteCentre">
            <br />
            
            <Container>
         
                <h1 className="text-center">Liste des reservations</h1>
          
            </Container>
            <br/>
            <RecupReservations idSalle={idSalle}/>
        </Container>
    );
}