import React, { useState, useEffect, Fragment } from 'react';
import { Container, Form, Col } from 'react-bootstrap';
import Menu from '../Composants/Menu';
import RecupBatiment from '../Composants/RecupBatiment';
import RecupTypeSalle from '../Composants/RecupTypeSalle';
import { Redirect } from 'react-router-dom';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function CreationSalle() {
    const [salle, setSalle] = useState(null);
    const [pret, setPret] = useState(false);
    const [nom, setNom] = useState("");
    const [type, setType] = useState("0");
    const [batiment, setBatiment] = useState("0");
    const [numero, setNumero] = useState("");
    const [surface, setSurface] = useState(0);
    const [capacite, setCapacite] = useState(0);
    const [status, setStatus] = useState(false);
    const [codeRole, setCodeRole] = useState();

    let idSalle = document.URL.split('=')[1];


    const getSalle = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/vrcRest?idSalle=${idSalle}`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => {
                setSalle(json.objetRetourne);
                setCodeRole(json.codeRole);
            })
            .catch(error => { console.log(error); })
    }

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handleNumero = (evt) => {
        setNumero(evt.target.value);
    }

    const handleSurface = (evt) => {
        setSurface(evt.target.value);
    }

    const handleCapacite = (evt) => {
        setCapacite(evt.target.value);
    }

    const handleType = (evt) => {
        setType(evt.target.value);
    }

    const handleBatiment = (evt) => {
        setBatiment(evt.target.value);
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getSalle();
            setPret(true);
        }
        fetchJson();
        getSalle();
    }, []);
    if (status) {
        return (<Redirect to="listeSalles" />);
    }
    else if (pret && codeRole != 200) {
        return (<Redirect to=""/>);
    }
    else if (pret && salle !== null) {
        return (
            <div>
                <Menu />
                <Container>
                    <br />
                    <div className="fond">
                        <h1 className="text-center">Modification de la salle<Form.Label id="idSalle"></Form.Label></h1>
                        <br />
                        <Container>
                            <Form>
                                <Form.Group>
                                    <Form.Label htmlFor="batiment">Nom de Bâtiment</Form.Label><br />
                                    <RecupBatiment id="batiment" valeur={salle.batiment.nom} handleBatiment={handleBatiment}/>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label htmlFor="type">Type de salle</Form.Label><br />
                                    <RecupTypeSalle id="type" valeur={salle.typeSalle} handleType={handleType}/>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label htmlFor="nom">Nom de salle</Form.Label>
                                    <Form.Control id="nom" type="text" placeholder="ex : Watch Tower" defaultValue={salle.nom} onChange={handleNom} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label htmlFor="numero">Numéro de salle</Form.Label>
                                    <Form.Control id="numero" type="text" placeholder="ex : 704" defaultValue={salle.numero} onChange={handleNumero} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label htmlFor="surface">Surface (en m²)</Form.Label>
                                    <Form.Control id="surface" type="number" step="0.001" placeholder="ex : 100" defaultValue={salle.surface} onChange={handleSurface} />
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label htmlFor="capacite">Capacite</Form.Label>
                                    <Form.Control id="capacite" type="number" step="1" placeholder="ex : 25" defaultValue={salle.capacite} onChange={handleCapacite} />
                                </Form.Group>
                                <br />
                                {materiel(salle.listeMateriels)}
                                
                                <br />
                                <input className="btn btn-info mr-5" type="button" value="Valider" onClick={() => modifierSalle(salle, nom, batiment, type, numero, surface, capacite, setStatus, "valider")} />
                                <input className="btn btn-outline-danger" type="button" value="Supprimer" onClick={() => supprimerSalle(setStatus)} />
                                <br />
                            </Form>
                        </Container>
                    </div>
                </Container>
            </div>
        )
    }
    else {
        return (<div></div>);
    }

}

function materiel(salle) {
    return (
        <Col md="auto" className="tableauxSalle col-md-6 col-sm-12">
            <table className="text-center">
                <thead>
                    <tr>
                        <th>Matériel</th>
                        <th>Quantité</th>
                        <th><input className="btn btn-info" type="button" value="nouveau type de materiel" onClick={() => ajoutMateriel()} /></th>
                    </tr>
                </thead>
                <tbody id="t">
                    {salle.map((materiel) =>
                        <tr>
                            <td>{materiel.type.type}</td>
                            <td>
                                <input name={materiel.type.type} id={materiel.id} type="number" defaultValue={materiel.quantite}></input>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </Col>
    );
}

function ajoutMateriel(){
    document.location.href = "/ajoutMateriel";
}

function modifierSalle(salle, nom, batiment, type, numero, surface, capacite, setStatus, modif) {
    let idSalle = parseInt(document.URL.split("=")[1]);
    let materiels = document.getElementById("t").getElementsByTagName("input");
    let mats = "";
    console.log(materiels)
    for (let mat of materiels) {
        let id = salle.listeMateriels.find((materiel) => {
            return materiel.id==mat.id
        })
        mats = mats + "{\"id\": " + mat.id + ",\"type\": {\"id\": " + id.id + ",\"type\": \"" + id.type + "\"}" + ",\"quantite\":" + mat.value + "},"
    }
    mats = mats.substr(0, mats.length - 1) + "";
    fetch(`${process.env.REACT_APP_API_URL}/MSRest`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
            "nomsalle": "${ nom}",
            "numsalle": "${ numero}",
            "surface": "${ surface}",
            "capacite": "${ capacite}",
            "type": "${ type}",
            "modif": "${ modif}",
            "id": "${idSalle}",
            "batiment": "${batiment}",
            "listeMateriels": [${mats}]
            }`
    })
        .then(response => {
            let temp = response.json();
            setStatus(true);
            return temp
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })

}

function supprimerSalle(setStatus) {
    let idSalle = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/MSRest?idSalle=${idSalle}`, {
        method: "delete",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => {
            return response.json();
        })
        .then(json => setStatus(json.objetRetourne))
        .catch(error => {
            console.log(error);
            setStatus(false);
        })
}