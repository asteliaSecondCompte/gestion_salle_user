import React, { useState, useEffect } from 'react';
import RecupReservs from '../Composants/RecupReservs';
import { Redirect, Link } from 'react-router-dom';
import Menu from '../Composants/Menu';
import { Container } from 'react-bootstrap';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function ModifierReserv() {

    const [reserv, setReserv] = useState();
    const [intitule, setIntitule] = useState("");
    const [dateDebut, setDateDebut] = useState();
    const [duree, setDuree] = useState(1);
    const [pret, setPret] = useState(false);
    const [status, setStatus] = useState(false);
    const [codeRole, setCodeRole] = useState();

    let idSalle = document.URL.split("?")[1].split("&&")[0].split("=")[1];
    let idReserv = document.URL.split("?")[1].split("&&")[1].split("=")[1];

    const handleIntitule = (evt) => {
        setIntitule(evt.target.value);
    }

    const handleDateDebut = (evt) => {
        setDateDebut(evt.target.value);
    }

    const handleDuree = (evt) => {
        setDuree(evt.target.value);
    }

    const getReserv = () => {
        fetch(`${process.env.REACT_APP_API_URL}/choixReserv?idReserv=${idReserv}`, {
            method : "get",
            headers: {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => {
                setReserv(json.objetRetourne);
                setIntitule(json.objetRetourne.intitule);
                setDateDebut(conversionDate(json.objetRetourne.dateDebut.dayOfMonth, 
                    json.objetRetourne.dateDebut.monthValue, 
                    json.objetRetourne.dateDebut.year));
                setCodeRole(json.codeRole)
            })
            .catch(error => console.log(error))
    }


    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getReserv();
            setPret(true);
        }
        fetchJson();
        getReserv();
    }, []);

    const modifierReserv = () => {
        fetch(`${process.env.REACT_APP_API_URL}/ModifReservRest`, {
            method: "put",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Accept: 'application/json',
                "Authorization": sessionStorage.token
            },
            body: `{
                        "idReservation" : ${ parseInt(idReserv)},
                        "intitule" : "${ intitule}",
                        "dateDebut" : "${ dateDebut}",
                        "duree" : "${ duree}",
                        "idSalle" : "${ idSalle}"
                    }`
        })
            .then(response => response.json())
            .then(json => {
                setStatus(json.objetRetourne)
            })
            .catch(error => {
                setStatus(-1);
                console.log(error);
            })
    }

    const annulerReserv = () => {
        fetch(`${process.env.REACT_APP_API_URL}/AnnulerRest?idReservation=${idReserv}`, {
            method: "delete",
            headers: {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => setStatus(json.objetRetourne))
            .catch(error => {
                setStatus(-1);
                console.log(error);
            })
    }

    console.log("status : "+status)
    console.log("pret : "+pret)
    console.log(reserv)
    if (status === true) {
        return (<Redirect to={"reserverSalle?idSalle=" + idSalle} />);
    }
    else if (pret && codeRole != 200) {
        return <Redirect to=""/>
    }
    else if (status === false && pret && reserv !== undefined) {
        return (<div>
            <Menu />
            <br />
            <Container className="fond">
                <h2>Réservation à modifier </h2>
                <br />
                <RecupReservs idSalle={idSalle} />
                <br />
                <div>
                    <label htmlFor="intitule">Intitule</label>
                    <input id="intitule" className="form-control" type="text" defaultValue={intitule} onChange={handleIntitule} />
                </div>
                <div>
                    <label htmlFor="dateDebut">Date de début</label>
                    <input id="dateDebut" className="form-control" type="date"
                        defaultValue={conversionDate(reserv.dateDebut.dayOfMonth, reserv.dateDebut.monthValue, reserv.dateDebut.year)}
                        onChange={handleDateDebut} />
                </div>
                <div>
                    <label htmlFor="duree">Durée</label>
                    <input id="duree" className="form-control" type="number" min="1" step="1" defaultValue="1" onChange={handleDuree} />
                </div>
                <input className="btn btn-success" type="button" value="Valider" onClick={modifierReserv} />
                <input className="btn btn-danger" type="button" value="Supprimer" onClick={annulerReserv} />
            </Container>
        </div>);
    }
    else {
        return (<div>
            <Menu/>
            <Container>
            <br />
            <label>La reservation demandée ne peut être modifiée ou n'existe pas !</label>
            <br />
            <Link to={"reserverSalle?idSalle=" + idSalle}>Retour à la réservation de la salle</Link>
            </Container>
        </div>);
    }

}

function conversionDate(jour, mois, annee) {
    let sJour = "" + jour;
    let sMois = "" + mois;
    let sAnnee = "" + annee;
    if (sJour.length < 2) {
        sJour = 0 + sJour;
    }
    if (sMois.length < 2) {
        sMois = 0 + sMois;
    }
    if (sAnnee.length < 4) {
        while (sAnnee.length) {
            sAnnee = 0 + sAnnee;
        }
    }
    return sAnnee + "-" + sMois + "-" + sJour;
}