import React, { useState, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import {CODE_NOAUTH, CODE_USER} from '../CodeRole';
import Menu from '../Composants/Menu';

export default function AjoutMateriel() {

    const [nom, setNom] = useState();
    const [status, setStatus] = useState(-1);
    const [codeRole, setCodeRole] = useState(202);

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    if (status === 0) {
        return (<Redirect to="" />);
    }
    else if (codeRole == 201) {
        return (<Redirect to="menuUser" />);
    }
    else if (status !== 0) {
        return <Fragment>
            (
                <div>
                <Menu/>
        <div className="fond" >
                <br /><br />
                <h1 class="text-center">Ajout d'un role</h1>
                <br />
                <div className="container" id="marge_haut">
                    <div className="col-sm-6">
                        <label for="nom">Nouveau role</label>
                        <br />
                        <br />
                        <input id="nom" className="form-control" type="text" placeholder="ex : formateur" onChange={handleNom} />
                    </div>
                    <br />
                    <Button className="btn btn-info" onClick={() => creerRole(nom, setStatus, setCodeRole)}>Créer</Button>
                    <Button className="btn btn-info" onClick={() => { document.location.href = "/creationCompteAdmin"; }}>Retour</Button>
                </div>

            </div>
            </div>
    </Fragment>
    }
}

function creerRole(nom, setStatus, setCodeRole) {
    fetch(`${process.env.REACT_APP_API_URL}/ARRest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
                "nom": "${ nom}"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.fait);
            setCodeRole(json.codeRole);
            if (json.fait == -2) {
                alert("Nom invalide");
            }
        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })
    window.history.back();
}

