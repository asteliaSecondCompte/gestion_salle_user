import React, { useState, useEffect } from 'react';
import Menu from '../Composants/Menu';
import { Col, Row, Container } from 'react-bootstrap';
import { useLocation, Link, Redirect } from 'react-router-dom';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function SalleComplete() {
    const [pret, setPret] = useState(false);
    const [salle, setSalle] = useState(null);
    const [codeRole, setCodeRole] = useState();

    let idSalle = document.URL.split('=')[1];

    const recupSalle = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/vrcRest?idSalle=${idSalle}`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => { setSalle(json.objetRetourne);
                setCodeRole(json.codeRole)
            })
            .catch(error => { console.log(error); })
    }

    const description = () => {
        return (
            <Row>
                <Col className=" col-md-12 col-sd-6 " id="marge_haut">

                    <label ><h4><u>DESCRIPTION :</u></h4> </label><br />
                    <label ><strong>Bâtiment :</strong> {salle.batiment.nom}</label><br />
                    <label><strong>Nom :</strong> {salle.nom}</label><br />
                    <label><strong>Surface :</strong> {salle.surface} m²</label><br />
                    <label><strong>Type :</strong> {salle.typeSalle}</label><br />
                    <label><strong>Capacité :</strong> {salle.capacite} personnes</label><br />
                    <br />
                    <span  ><h4><u>CONTENU DE LA SALLE :</u></h4></span>
                    <table id="materiely" className="text-center table table-borderless">
                        <thead>
                            <tr>
                                <th id="notbold">Matériel</th>
                                <th id="notbold">Quantité</th>
                            </tr>
                        </thead>

                        {materielSalle(salle.listeMateriels)}
                    </table>
                </Col>
            </Row>
        );
    }



    const reservations = () => {
        return (
            <Col md="auto" className="col-md-12 col-sd-6" id="marge_haut">
                <th className="bg-info col-md-2 text-center">Réservations</th>
                <table id="reservation" className="text-center table table-striped">
                    <thead>
                        <tr>
                        </tr>
                        <tr  >
                            <th >Intitule</th>
                            <th>Date de début</th>
                            <th>Date de fin</th>
                        </tr>
                    </thead>
                    {resaSalle(salle.listeReservations)}
                </table>
            </Col>
        )
    }

    const afficherSalle = () => {
        return (
            <Container>
                <Row>
                    <Col>{description()}</Col>
                    <Col>{reservations(salle)}</Col>
                </Row>
                <br />
            </Container>
        )
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recupSalle();
            setPret(true);
        }
        fetchJson();
        recupSalle();
    }, []);

    const menu = () => {
        if (codeRole == 200) {
            return <Menu />
        }
    }

    const retour = () => {
        if (codeRole == 201) {
            return <Link to="/menuUser">Retour</Link>
        }
    }

    if (pret && codeRole == 202) {
        return (<Redirect to=""/>)
    }
    else if (pret && salle != null) {
        return (
            <div>
                { menu() }
                <div className="fond">
                <br />
                <h1 className="text-center" id="titre">Salle {salle.nom}</h1>
                <div className="text-center">
                    <img  src="http://www.bellegarde-45.fr/sites/default/files/public/media/panes/p1110597.jpg" alt="Pas d'image" width="500px"/> 
                </div>
                {afficherSalle()}
                { retour() }
                </div>
            </div>
        );
    }
    else {
        return (
            <div>
                { menu }
                <Container>
                    <br />
                    <label className="fond">La salle demandée n'existe pas !</label>
                    <br />
                    { retour }
                </Container>
            </div>
        );
    }
}


function materielSalle(liste) {
    return (
        <tbody>
            {liste.map((materiel) =>
                <tr >
                    <td>{materiel.type.type}</td>
                    <td>{materiel.quantite}</td>
                </tr>
            )}
        </tbody>
    );
}


function resaSalle(liste) {
    return <tbody>
        {liste.map((resa) =>
            <tr key={resa.id}>
                <td>{resa.intitule}</td>
                <td>{conversionDate(resa.dateDebut.dayOfMonth, resa.dateDebut.monthValue, resa.dateDebut.year)}</td>
                <td>{conversionDate(resa.dateFin.dayOfMonth, resa.dateFin.monthValue, resa.dateFin.year)}</td>
            </tr>
        )}
    </tbody>
        ;
}

function conversionDate(jour, mois, annee) {
    let sJour = "" + jour;
    let sMois = "" + mois;
    let sAnnee = "" + annee;
    if (sJour.length < 2) {
        sJour = 0 + sJour;
    }
    if (sMois.length < 2) {
        sMois = 0 + sMois;
    }
    if (sAnnee.length < 4) {
        while (sAnnee.length) {
            sAnnee = 0 + sAnnee;
        }
    }
    return sJour + "/" + sMois + "/" + sAnnee;
}

