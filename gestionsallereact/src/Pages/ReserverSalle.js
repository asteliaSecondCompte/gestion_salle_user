import React, { useState, Component, useEffect } from "react";
import { Container, Col, Form, Row, Button } from "react-bootstrap";
import Menu from '../Composants/Menu';
import "./Page.css";
import ListeReservations from "./ListeReservations";
import { Redirect } from 'react-router-dom';

export default function ReserverSalle() {
  const [intitule, setIntitule] = useState();
  const [dateDebut, setDateDebut] = useState();
  const [duree, setDuree] = useState();
  const [pret, setPret] = useState(false);
  const [codeRole, setCodeRole] = useState()

  const handIntitule = evt => {
    setIntitule(evt.target.value);
  };

  const handDateDebut = evt => {
    setDateDebut(evt.target.value);
  };

  const handleDuree = evt => {
    setDuree(evt.target.value);
  };

  const verif = async () => {
    await fetch(`${process.env.REACT_APP_API_URL}/verifAutorisationRest`, {
      method: "get",
      headers: {
        "Authorization": sessionStorage.token
      }
    })
      .then(response => response.json())
      .then(json => {
        setCodeRole(json);
      })
      .catch(error => { console.log(error); })
  }

  useEffect(() => {
    const fetchJson = async () => {
      setPret(false);
      await verif();
      setPret(true);
    }
    fetchJson();
    verif();
  }, [])

  if (pret && codeRole != 200) {
    return (<Redirect to="" />);
  }
  else if (pret) {
    return (

      <div>
        <Menu />
        <Container className="fond">
          <br />
          <h1 className="text-center">Réserver la Salle <label id="idSalle"></label>
          </h1>
          <Container>


            <label htmlFor="intitule" id="marge_haut"><strong>Intitulé</strong></label>
            <input
              class="form-control col-12"
              name="intitule"
              type="text"
              placeholder="Entrer un intitulé"
              onChange={handIntitule}
            />


            <div class="form-group mr-2">
              <label htmlFor="dateDebut"><strong>Date de Début de la reservation</strong></label>
              <input
                class="form-control col-12"
                name="dateDebut"
                type="date"
                placeholder="Entrer une date de début"
                onChange={handDateDebut}
              />
            </div>

            <div class="form-group mr-2">
              <label htmlFor="duree"><strong>Durée de la reservation</strong></label>
              <input
                class="form-control col-12"
                name="duree"
                type="text"
                placeholder="Entrer une durée"
                onChange={handleDuree}
              /><br />
              <div class="text-center">
                <Button
                  className="btn btn-success  "
                  onClick={() => reserverSalle(intitule, dateDebut, duree)}
                >
                  Réserver
        </Button></div>
            </div>

            <br /><br />
            <ListeReservations />

          </Container>
        </Container>
      </div>
    );
  }
  else {
    return (<div></div>);
  }
}

function reserverSalle(intitule, dateDebut, duree) {
  let idSalle = parseInt(document.URL.split("=")[1]);
  fetch(`${process.env.REACT_APP_API_URL}/ReserverRest`, {
    method: "post",
    headers: {
      "Content-type": "application/json; charset=UTF-8",
      Accept: "application/json",
      "Authorization": sessionStorage.token
    },
    body: `{
            "idSalle": "${idSalle}",
                "intitule": "${intitule}",
                "debut": "${dateDebut}",
                "duree": "${duree}"
             
            }`
  })
    .then(response => {
      let temp = response.json();
      return temp;
    })
    .catch(error => {
      console.log(error);
    });
}

/*<form class ="Reservation">


            <MenuSalles />
            <br /><br />
            <h2 class="text-center">Réserver la Salle <label id="idSalle"></label></h2>
            <br />
            <div id="message"></div>
            <Container Col md="3" xs="9">
                <h6 class="text-center">Liste des réservations de la salle <label id="idSalle"></label></h6>

            </Container>
            {reservations()}
            <hr />


            <Form action class ="text-center""listeSalles">
                <Row>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="nom">Intitulé</Form.Label>
                        <div id="space"></div>
                        <Form.Control id="intitule" type="text" placeholder="ex : Watch Tower" />
                    </Col>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="numero">Date de début</Form.Label>
                        <Form.Control id="debut" type="date" />
                    </Col>
                    <Col md="3" xs="9">
                        <Form.Label htmlFor="surface">Date de fin</Form.Label>
                        <Form.Control id="fin" type="date" />
                    </Col>
                </Row>
                <br/>
                <a href="/listeSalles">
                    <input className="btn btn-success" type="button" value="Réserver" />
                </a>
            </Form>

</form>



    );
}

function reservations() {
    return (
        <Col className="tableauxSalle">
            <table id="reservation">
                <thead>
                    <tr>
                        <th>Intitulé</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Gotham Sirens</td>
                        <td>01/04/2020</td>
                        <td>05/04/2020</td>
                    </tr>
                </tbody>
            </table>
        </Col>
    )
}
}*/
