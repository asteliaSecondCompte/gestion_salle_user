import React from 'react';
import { Link } from 'react-router-dom';
import RecupSalles from '../Composants/RecupSalles';
import { Col } from 'react-bootstrap';

export default function MenuUser() {
    return (
        <div className="container texteCentre">
            <br />
            <Col md="12" id="marge_haut" className="fond">
                <br/>
                <h2>Bienvenue</h2>
                <br />
                <br />
                <RecupSalles />
                <br />
                <footer>
                    <Link to="/deconnexion"><input className="btn btn-danger" type="button" value="Déconnexion" /></Link>
                </footer>
                <br/>
            </Col>
        </div>
    );
}