import React from 'react';
import { Container } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';
import RecupMessages from '../Composants/RecupMessages';

export default function BoiteReception() {

    return (
        <div>
            <MenuMessagerie/>
        <Container>
            <br/>
            <h2 class="text-center">Boîte de Réception</h2>
            <br/>
            <RecupMessages value="recu" />
        </Container>
        </div>
    );

}