import React, { useEffect, Fragment } from 'react';
import Menu from '../Composants/Menu';
import { useState } from 'react';
import { Redirect, Link } from 'react-router-dom';
import RecupRoles from '../Composants/RecupRoles';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function ModifierUser() {

    const [user, setUser] = useState(null);
    const [pret, setPret] = useState(false);
    const [nom, setNom] = useState("");
    const [prenom, setPrenom] = useState("");
    const [email, setEmail] = useState("");
    const [adresse, setAdresse] = useState("");
    const [role, setRole] = useState("");
    const [dateN, setDateN] = useState("");
    const [mdp, setMDP] = useState("");
    const [mdpVerif, setMDPVerif] = useState("");
    const [status, setStatus] = useState(false);
    const [codeRole, setCodeRole] = useState();


    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN, setCodeRole);
            setPret(true);
        }
        fetchJson();
        getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN, setCodeRole);
    }, []);

    console.log(codeRole)
    return <Fragment>
        { (pret && codeRole != undefined && codeRole != 200) ? (<Redirect to=""/>) :  
            (status ? (<Redirect to="listePersonnes" />) :
                ((pret && user !== null) ?
                    (
                        <div className="fond">
                        <Menu />
                        <div className="container">
                            <br />
                            <br />
                            <h2>Modification d'un utilisateur</h2>
                            <br />
                            <div className="container">
                                <form className="form-group">
                                    <div>
                                        <label htmlFor="nom">Nom</label>
                                        <input id="nom" className="form-control" type="text" defaultValue={user.nom} onChange={handleNom} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="prenom">Prénom</label>
                                        <input id="prenom" className="form-control" type="text" defaultValue={user.prenom} onChange={handlePrenom} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="email">Email</label>
                                        <input id="email" className="form-control" type="mail" defaultValue={user.email} onChange={handleEmail} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="adresse">Adresse</label>
                                        <input id="adresse" className="form-control" type="text" defaultValue={user.adresse} onChange={handleAdresse} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="role">Rôle</label><br />
                                        <RecupRoles id="role" valeur={user.role} handleRole={ handleRole } />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="dateN">Date de naissance</label>
                                        <input id="dateN" className="form-control" type="date" defaultValue={conversionDate(user.dateNaissance.dayOfMonth, user.dateNaissance.monthValue, user.dateNaissance.year)}
                                            onChange={handleDateN} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="mdp">Mot de passe</label>
                                        <input id="mdp" className="form-control" type="password" onChange={handleMDP} />
                                    </div>
                                    <br />
                                    <div>
                                        <label htmlFor="mdpVerif">Valider mot de passe</label>
                                        <input id="mdpVerif" className="form-control" type="password" onChange={handleMDPVerif} />
                                    </div>
                                    <br />
                                    <tr>
                                        <td></td>

                                    </tr>
                                    <input className="btn btn-info btn-inline" type="button" value="Valider" onClick={() => modifierUtilisateur(nom, prenom, email, adresse, role, dateN, mdp, mdpVerif, setStatus, setCodeRole)} />
                                    <input className="btn btn-primary" type="button" value="Activer/Désactiver" onClick={() => activerDesactiver(setStatus, setCodeRole)} />
                                    <input className="btn btn-danger" type="button" value="Supprimer" onClick={ () => supprimerUtilisateur(setStatus, setCodeRole) } />
                                    <br/>
                                </form>
                            </div>
                        </div>
                        </div>
                    ) : (<div className="fond">
                            <label>La personne indiquée ne peut être modifiée ou n'existe pas !</label>
                            <br/>
                            <Link to="choixUser">Retour au choix d'utilisateurs</Link>
                        </div>
                         )))} 
                         
    </Fragment>
}

function conversionDate(jour, mois, annee) {
    let sJour = "" + jour;
    let sMois = "" + mois;
    let sAnnee = "" + annee;
    if (sJour.length < 2) {
        sJour = 0 + sJour;
    }
    if (sMois.length < 2) {
        sMois = 0 + sMois;
    }
    if (sAnnee.length < 4) {
        while (sAnnee.length) {
            sAnnee = 0 + sAnnee;
        }
    }
    return sAnnee + "-" + sMois + "-" + sJour;
}

function supprimerUtilisateur(setStatus, setCodeRole) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMURest?idUser=${idUser}`, {
        method: "delete",
        headers: {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => {
            return response.json();
        })
        .then(json => {
            setStatus(json.objetRetourne)
            setCodeRole(json.codeRole);
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })
}

function activerDesactiver(setStatus, setCodeRole) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMUActifRest?idUser=${idUser}`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        }
    })
        .then(response => {
            return response.json();
        })
        .then(json => {
            setStatus(json.objetRetourne)
            setCodeRole(json.codeRole);
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })
}

function modifierUtilisateur(nom, prenom, email, adresse, role, dateN, mdp, mdpVerif, setStatus, setCodeRole) {
    let idUser = parseInt(document.URL.split("=")[1]);
    fetch(`${process.env.REACT_APP_API_URL}/SMURest`, {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
                "id": ${idUser},
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "datenaissance": "${ dateN}",
                "role": "${ role}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.objetRetourne)
            setCodeRole(json.codeRole);
        })
        .catch(error => {
            console.log(error);
            setStatus(false);
        })

}

async function getUser(setUser, setNom, setPrenom, setEmail, setAdresse, setRole, setDateN, setCodeRole) {
    let idUser = document.URL.split("=")[1];
    await fetch(`${process.env.REACT_APP_API_URL}/SChURest?idUser=${idUser}`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => response.json())
        .then(json => {
            setUser(json.objetRetourne);
            setCodeRole(json.codeRole);
            setNom(json.objetRetourne.nom);
            setPrenom(json.objetRetourne.prenom);
            setEmail(json.objetRetourne.email);
            setAdresse(json.objetRetourne.adresse);
            setRole(json.objetRetourne.role);
            setDateN(conversionDate(json.objetRetourne.dateNaissance.dayOfMonth, 
                 json.objetRetourne.dateNaissance.monthValue, 
                 json.objetRetourne.dateNaissance.year));
        })
        .catch(error => console.log(error))
}
