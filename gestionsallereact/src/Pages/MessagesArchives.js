import React from 'react';
import { Container } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';
import RecupMessages from '../Composants/RecupMessages';

export default function MessagesArchives() {

    return (
        <div>
            <MenuMessagerie/>
        <Container>
            <br/>
            <h2 className="text-center">Messages Archives</h2>
            <br/>
            <RecupMessages value="archive" />
        </Container>
        </div>
    );

}