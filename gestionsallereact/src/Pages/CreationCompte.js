import React, { useState, Fragment } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { useToasts } from 'react-toast-notifications';

export default function CreationCompte() {

    const [nom, setNom] = useState();
    const [prenom, setPrenom] = useState();
    const [email, setEmail] = useState();
    const [adresse, setAdresse] = useState();
    const [role, setRole] = useState("Stagiaire");
    const [dateN, setDateN] = useState();
    const [login, setLogin] = useState();
    const [mdp, setMDP] = useState();
    const [mdpVerif, setMDPVerif] = useState();
    const [status, setStatus] = useState(-1);
    const { addToast } = useToasts();

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }
    if (status === 0) {
        return (<Redirect to="" />);
    }
    else if (status !== 0) {
        return <Fragment>
            <div  >
                <br /><br />
                <div className="fond">
                    <h1 class="text-center">Création d'un compte</h1>
                    <br />
                    <div className="container">
                        <form action="http://localhost:3000/" className="form-group">
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="nom">Nom</label>
                                    <input id="nom" className="form-control" type="text" placeholder="ex : Wayne" onChange={handleNom} />
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="prenom">Prénom</label>
                                    <input id="prenom" className="form-control" type="text" placeholder="ex : Bruce" onChange={handlePrenom} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="email">Email</label>
                                    <input id="email" className="form-control" type="mail" placeholder="ex : batman@gmal.com" onChange={handleEmail} />
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="adresse">Adresse</label>
                                    <input id="adresse" className="form-control" type="text" placeholder="ex : Manoir Wayne, Gotham City" onChange={handleAdresse} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="role">Rôle</label><br />
                                    <select id="role" className="form-control" onChange={handleRole}>
                                        <option id="stagiaire">Stagiaire</option>
                                        <option id="formateur">Formateur</option>
                                    </select>
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="dateN">Date de naissance</label>
                                    <input id="dateN" className="form-control" type="date" placeholder="ex : 01/04/1998" onChange={handleDateN} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="login">Login</label>
                                    <input id="login" className="form-control" type="text" placeholder="ex : batman" onChange={handleLogin} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="mdp">Mot de passe</label>
                                    <input id="mdp" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDP} />
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="mdpVerif">Valider mot de passe</label>
                                    <input id="mdpVerif" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDPVerif} />
                                </div>
                            </div>
                            <br />
                            <Button className="btn btn-info" onClick={() => creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast)}>Créer</Button>
                            <span className="container">
                                <Link to=""><input className="btn btn-outline-info" type="button" value="Retour" /></Link>
                            </span>

                        </form>
                        <br />
                    </div>
                </div>
            </div>
    )}
    </Fragment>
    }
}

function creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast) {
    fetch(`${process.env.REACT_APP_API_URL}/SCURest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "role": "${ role}",
                "dateNaissance": "${ dateN}",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "pageUser"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.codeRetour);
            switch (json.codeRetour) {
                case -2:
                    addToast("Nom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -3:
                    addToast("Prenom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -4:
                    addToast("Mail invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -6:
                    addToast("Date de naissance invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -7:
                    addToast("Login invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -8:
                    addToast("Mot de Passe différent", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -9:
                    addToast("Veuillez remplir tous les champs", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case 0:
                    addToast("Compte créer", {
                        appearance: 'success',
                        autoDismiss: true,
                    });
                    break;
                default:
            }
        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })
}

