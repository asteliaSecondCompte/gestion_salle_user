import React, { Fragment, useState } from 'react';
import Menu from '../Composants/Menu';
import { Redirect } from 'react-router-dom';
import RecupPersonnesModif from '../Composants/RecupPersonnesModif';

export default function ChoixUser() {
    const [val, setVal] = useState();
    const [user, setUser] = useState(null);

    /*const inputValue = (evt) => {
        setVal(evt.target.value)
    }

    const handleModif = () => {
        fetch(`${process.env.REACT_APP_API_URL}/SChURest?idUser=${val}`)
            .then(response => response.json())
            .then(json => {
                setUser(json);
            })
            .catch(error => console.log(error))
    }*/


    return <Fragment>
        {(user === null || user === {}) ?
            (<div>
                    <Menu />
                <div className="container fond">
                    <br />
                    <h1 className="text-center">Choix d'un utilisateur</h1>
                    <div>
                        <RecupPersonnesModif filtre="true" />
                    </div>
                    </div>
                </div>
            ) : ( <Redirect to={"modifierUser?idUser="+ val} /> )}
    </Fragment>

}

