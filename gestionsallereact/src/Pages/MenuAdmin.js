import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';

export default function MenuAdmin() {

    const [pret, setPret] = useState(false);
    const [codeRole, setCodeRole] = useState()

    const verif = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/verifAutorisationRest`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => { 
                setCodeRole(json);
            })
            .catch(error => { console.log(error); })
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await verif();
            setPret(true);
        }
        fetchJson();
        verif();
    },[])


    if (pret && codeRole != 200) {
        return (<Redirect to=""/>);
    }
    else if (pret) {
        return (
            <Container>
                <br/>
                <Row md="8" className="justify-content-center align-items-center">
                    <Col md="8" className="fond">
                        <div className="form-group">
                            <br/>
                            <h2>Menu Administrateur</h2>
                            <br />
                            <Container>
                                <Link to="/listeSalles">Gérer les salles</Link><br /><br />
                                <Link to="/listePersonnes">Gérer les utilisateurs</Link><br /><br />
                                <Link to="/deconnexion"><input className="btn btn-danger" type="button" value="Déconnexion" /></Link>
                                <br />
                            </Container>
                        </div>
                    </Col>
                    <br />
                </Row>
            </Container>
        );
    }
    else {
        return (<div></div>);
    }
    
}