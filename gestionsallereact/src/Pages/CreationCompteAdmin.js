import React, { useState, Fragment, useEffect } from 'react';
import Menu from '../Composants/Menu';
import { Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import RecupRoles from '../Composants/RecupRoles';
import { useToasts } from 'react-toast-notifications';

export default function CreationCompteAdmin() {
    const [nom, setNom] = useState();
    const [prenom, setPrenom] = useState();
    const [email, setEmail] = useState();
    const [adresse, setAdresse] = useState();
    const [role, setRole] = useState();
    const [dateN, setDateN] = useState();
    const [login, setLogin] = useState();
    const [mdp, setMDP] = useState();
    const [mdpVerif, setMDPVerif] = useState();
    const [status, setStatus] = useState(-1);
    const { addToast } = useToasts();
    const [pret, setPret] = useState(false);
    const [codeRole, setCodeRole] = useState()

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handlePrenom = (evt) => {
        setPrenom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleAdresse = (evt) => {
        setAdresse(evt.target.value);
    }

    const handleRole = (evt) => {
        setRole(evt.target.value);
    }

    const handleDateN = (evt) => {
        setDateN(evt.target.value);
    }

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }

    const verif = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/verifAutorisationRest`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => { 
                setCodeRole(json);
            })
            .catch(error => { console.log(error); })
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await verif();
            setPret(true);
        }
        fetchJson();
        verif();
    },[])

    if (status === 0) {
        return (<Redirect to="listePersonnes" />);
    }
    else if (pret && codeRole != 200) {
        return (<Redirect to=""/>);
    }
    else if (pret) {
        return <Fragment>
            {
                (<div>
                    <Menu />
                    <div className="container fond">
                        <br />
                        <h1 class=" text-center">Création d'un compte</h1>
                        <div className="container"  >
                            <form id="compte" className="form-group">
                                <div className="row">
                                    <div className="col-sm-4">
                                        <label htmlFor="nom">Nom</label>
                                        <input id="nom" className="form-control" type="text" placeholder="ex : Wayne" onChange={handleNom} />
                                    </div>
                                    <br />
                                    <div className="col-sm-4">
                                        <label htmlFor="prenom">Prénom</label>
                                        <input id="prenom" className="form-control" type="text" placeholder="ex : Bruce" onChange={handlePrenom} />
                                    </div>
                                    <div className="col-sm-4">
                                        <label htmlFor="dateN">Date de naissance</label>
                                        <input id="dateN" className="form-control" type="date" placeholder="ex : 01/04/1998" onChange={handleDateN} />
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-sm-4">
                                        <label htmlFor="email">Email</label>
                                        <input id="email" className="form-control" type="mail" placeholder="ex : batman@gmal.com" onChange={handleEmail} />
                                    </div>
                                    <div className="col-sm-8">
                                        <label htmlFor="adresse">Adresse</label>
                                        <input id="adresse" className="form-control" type="text" placeholder="ex : Manoir Wayne, Gotham City" onChange={handleAdresse} />
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-sm-6">
                                        <label htmlFor="role">Rôle</label><br />
                                        <RecupRoles id="role" handleRole={handleRole} setRole={setRole.bind(this)} />
                                    </div>
                                    <div className="col-sm-6">
                                        <br/>
                                        <Button className="btn btn-info mr-5" onClick={() => creerRole()}>Créer un role</Button>
                                    </div>
                                </div>
                                <br />
                                <div className="row">
                                    <div className="col-sm-4">
                                        <label htmlFor="login">Login</label>
                                        <input id="login" className="form-control" type="text" placeholder="ex : batman" onChange={handleLogin} />
                                    </div>
                                    <div className="col-sm-4">
                                        <label htmlFor="mdp">Mot de passe</label>
                                        <input id="mdp" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDP} />
                                    </div>
                                    <br />
                                    <div className="col-sm-4">
                                        <label htmlFor="mdpVerif">Valider mot de passe</label>
                                        <input id="mdpVerif" className="form-control" type="password" placeholder="ex : batarang" onChange={handleMDPVerif} />
                                    </div>
                                </div>
                                <br /><br />
                                <div class="text-center ">
                                    <Button className="btn btn-info mr-5" onClick={() => creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast)}>Créer utilisateur</Button>

                                    <Button className="btn btn-info" onClick={() => creerAdministrateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast)}>Créer administrateur</Button>
                                </div>
                                <br />
                            </form>
                        </div>
                    </div>
                </div>
                )}
        </Fragment>
    }
    else {
        return (<div></div>);
    }
}

function creerRole() {
    document.location.href = "/ajoutRole";
}

function creerUtilisateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast) {

    fetch(`${process.env.REACT_APP_API_URL}/SCURest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "role": "${ role}",
                "dateNaissance": "${ dateN}",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "user"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.codeRetour);
            switch (json.codeRetour) {
                case -2:
                    addToast("Nom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -3:
                    addToast("Prenom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -4:
                    addToast("Mail invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -6:
                    addToast("Date de naissance invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -7:
                    addToast("Login invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -8:
                    addToast("Mot de Passe différent", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -9:
                    addToast("Veuillez remplir tous les champs", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case 0:
                    addToast("Compte créer", {
                        appearance: 'success',
                        autoDismiss: true,
                    });
                    break;
                default:
            }

        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })

}

function creerAdministrateur(nom, prenom, email, adresse, role, dateN, login, mdp, mdpVerif, setStatus, addToast) {

    fetch(`${process.env.REACT_APP_API_URL}/SCURest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
                "nom": "${ nom}",
                "prenom": "${ prenom}",
                "mail": "${ email}",
                "adresse": "${ adresse}",
                "role": "${ role}",
                "dateNaissance": "${ dateN}",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "admin"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.codeRetour);
            switch (json.codeRetour) {
                case -2:
                    addToast("Nom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -3:
                    addToast("Prenom invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -4:
                    addToast("Mail invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -6:
                    addToast("Date de naissance invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -7:
                    addToast("Login invalide", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -8:
                    addToast("Mot de Passe différent", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case -9:
                    addToast("Veuillez remplir tous les champs", {
                        appearance: 'error',
                        autoDismiss: true,
                    });
                    break;
                case 0:
                    addToast("Compte créer", {
                        appearance: 'success',
                        autoDismiss: true,
                    });
                    break;
                default:
            }

        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })
}