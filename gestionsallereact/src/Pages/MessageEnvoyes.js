import React from 'react';
import { Container } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';
import RecupMessages from '../Composants/RecupMessages';

export default function MessagesEnvoyes() {

    return (
        <div>
            <MenuMessagerie/>
        <Container>
            <br/>
            <h2 className="text-center">Messages Envoyés</h2>
            <br/>
            <RecupMessages value="envoye" />
        </Container>
        </div>
    );

}