import React, { useState, useEffect } from 'react';
import Menu from '../Composants/Menu';
import RecupSalles from '../Composants/RecupSalles';
import { Redirect } from 'react-router-dom';

function ListeSalles() {

    const [pret, setPret] = useState(false);
    const [codeRole, setCodeRole] = useState()

    const verif = async () => {
        await fetch(`${process.env.REACT_APP_API_URL}/verifAutorisationRest`, {
            method: "get",
            headers: {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => {
                setCodeRole(json);
            })
            .catch(error => { console.log(error); })
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await verif();
            setPret(true);
        }
        fetchJson();
        verif();
    }, [])

    if (pret && codeRole != 200) {
        return (<Redirect to="" />);
    }
    else if (pret) {
        return (
            <div>
                <Menu />
                <br />
                <div className="container fond">
                    <br />
                    <h1 className="text-center" id="titre">Liste des salles</h1><br />
                    <div>
                        <RecupSalles />
                    </div>
                </div>
            </div>
        );
    }
    else {
        return (<div></div>);
    }
}

export default ListeSalles;


