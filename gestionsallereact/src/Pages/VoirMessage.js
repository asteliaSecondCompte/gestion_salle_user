import React from 'react';
import { Row, Col, Container, Form } from 'react-bootstrap';
import MenuMessagerie from '../Composants/MenuMessagerie';

export default function VoirMessage(props) {

    const message = recupMessage(props.value);

    return (
        <Container>
            <MenuMessagerie />
            <br />
            <h2 className="texteCentre">{"Planque du Pingouin"}</h2>
            <br />
            <Row>
                <Col md="auto" xs="4">
                    <Form.Label><h4>{"nightwing"}</h4></Form.Label><br />
                    <Form.Label><h6>{"A " + "moi"}</h6></Form.Label>
                </Col>
                <Col md="auto" xs="4">
                    <Form.Label>{"le " + "02/02/2020"}</Form.Label>
                </Col>
            </Row>
            <hr />
            <Row>
                <Col md="9" xs="9">
                    {"J'ai trouvé l'une des planques du Pingouin au salon de l'Iceberg. \n Rejoins moi sur le toit d'en face."}
                </Col>
            </Row>
        </Container>
    );

}

function recupMessage(id) {
    return;
}