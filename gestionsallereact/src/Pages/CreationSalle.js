import React, { useState, Fragment } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Form, Button } from 'react-bootstrap';
import Menu from '../Composants/Menu';
import RecupBatiment from '../Composants/RecupBatiment'
import RecupTypeSalle from '../Composants/RecupTypeSalle';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function CreationSalle() {
    const [batiment, setBatiment] = useState(1);
    const [type, setType] = useState(1);
    const [nom, setNom] = useState("");
    const [numero, setNumero] = useState("");
    const [image, setImage] = useState("");
    const [surface, setSurface] = useState(1);
    const [capacite, setCapacite] = useState(1);
    const [status, setStatus] = useState(-1);
    const [codeRole, setCodeRole] = useState();

    const handleBatiment = (evt) => {
        setBatiment(evt.target.value);
    }

    const handleType = (evt) => {
        setType(evt.target.value);
    }


    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handleNumero = (evt) => {
        setNumero(evt.target.value);
    }

    const handleImage = (evt) => {
        setImage(evt.target.value);
    }

    const handleSurface = (evt) => {
        setSurface(evt.target.value);
    }

    const handleCapacite = (evt) => {
        setCapacite(evt.target.value);
    }

    if (status === true) {
        return (<Redirect to="listeSalles" />);
    }
    else if (status !== true) {
        return <Fragment>
            {(
                <div>
                    <Menu />
                    <Container className="fond">
                        <br />
                        <h1 class="text-center" id="titre">Création d'une salle</h1>
                        <br />
                        <Container>
                            <Form action="listeSalles">
                                <div className="row" id="marge_haut">
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <Form.Label htmlFor="batiment"><strong>Nom de Bâtiment</strong></Form.Label><br />

                                            <RecupBatiment handleBatiment={handleBatiment} />

                                        </Form.Group>
                                    </div>
                                    <div className="col-sm-6" >
                                        <Form.Group>
                                            <Form.Label htmlFor="type"><strong>Type de salle</strong></Form.Label><br />

                                            <RecupTypeSalle handleType={handleType} />
                                        </Form.Group>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <Form.Label htmlFor="nom"><strong>Nom de salle</strong></Form.Label>
                                            <Form.Control id="nom" type="text" maxLength="4" placeholder="ex : CDA1" onChange={handleNom} />
                                        </Form.Group>
                                    </div>
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <Form.Label htmlFor="numero"><strong>Numéro de salle</strong></Form.Label>
                                            <Form.Control id="numero" type="text" min="0" maxLength="4" step="1" placeholder="ex : C001" onChange={handleNumero} />
                                        </Form.Group>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <Form.Label htmlFor="surface"><strong>Surface (en m²)</strong></Form.Label>
                                            <Form.Control id="surface" type="number" min="0" step="0.001" placeholder="1" onChange={handleSurface} />
                                        </Form.Group>
                                    </div>
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <Form.Label htmlFor="capacite"><strong>Capacite</strong></Form.Label>
                                            <Form.Control id="capacite" type="number" min="0" step="1" placeholder="1" onChange={handleCapacite} />
                                        </Form.Group>
                                    </div>
                                    
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <Form.Group>
                                            <input type="file"  id="fileToUpload" accept="image/png, image/jpeg" onChange={handleImage}/>
                                        </Form.Group>
                                    </div>
                                </div>
                                <Button className="btn btn-info mx-auto" id="marge_haut" onClick={() => creerSalle(batiment, type, nom, numero,image, surface, capacite, setStatus)} >Créer</Button>
                                <br />
                                <br />
                            </Form>
                        </Container>
                    </Container>
                </div>
            )}
        </Fragment>
    }
}
function creerSalle(batiment, type, nom, numero,image, surface, capacite, setStatus, setCodeRole) {
    var filename = image.replace(/^.*\\/, "");
    var ulrImage = "C:/ENV/img/"+filename;
    fetch(`${process.env.REACT_APP_API_URL}/asbddRest`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json',
            "Authorization": sessionStorage.token
        },
        body: `{
                "batiment": "${ batiment}",
                "type": "${ type}",
                "nomsalle": "${ nom}",
                "numsalle": "${ numero}",
                "image": "${ ulrImage}",
                "surface": "${ surface}",
                "capacite": "${ capacite}"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.objetRetourne);
            setCodeRole(json.codeRole)
            if (!json.objetRetourne) {
                alert("Veuillez remplir tous les champs");
            }
        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })

}
