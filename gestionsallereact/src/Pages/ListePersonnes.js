import React from 'react';
import RecupPersonnes from '../Composants/RecupPersonnes';
import Menu from '../Composants/Menu';
import { Container } from 'react-bootstrap';

export default function ListePersonnes() {
    return (
        <div>
            <Menu />
            <br />
            <Container className="fond">
                <br/>
                <div>
                    <h1 className="text-center">Liste des personnes</h1>
                </div>
                <br />
                <RecupPersonnes />
                </Container>
        </div>
    );
}