import React, { Fragment, useState } from 'react';
import Menu from '../Composants/Menu';
import RecupSallesModif from '../Composants/RecupSallesModif';
import { Redirect } from 'react-router-dom';

export default function ChoixSalle() {
    const [val, setVal] = useState();
    const [salle, setSalle] = useState(null);

    const handleModif = () => {
        fetch(`${process.env.REACT_APP_API_URL}/SChSRest?idSalle=${val}`)
            .then(response => response.json())
            .then(json => { setSalle(json); })
            .catch(error => console.log(error))
    }


    return <Fragment>
        {(salle === null || salle === {}) ?
            (<div >
                <Menu />
                <br />
                <div className="container">
                    <div className="fond" id ="marge_haut">
                        <h1 className="text-center ">Choix d'une salle</h1><br />
                        <div>
                            <RecupSallesModif />
                        </div>
                    </div>
                </div>
            </div>
            ) : (<Redirect to={"modifierSalle?salleId=" + val} />)}
    </Fragment>
}