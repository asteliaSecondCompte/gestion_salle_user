import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Authentification from '../Pages/Authentification';
import CreationCompte from '../Pages/CreationCompte';
import MenuAdmin from '../Pages/MenuAdmin';
import ListeSalles from '../Pages/ListeSalles';
import SalleComplete from '../Pages/SalleComplete';
import CreationSalle from '../Pages/CreationSalle';
import ChoixSalle from '../Pages/ChoixSalle';
import ReserverSalle from '../Pages/ReserverSalle';
import ModifierSalle from '../Pages/ModifierSalle';
import ListePersonnes from '../Pages/ListePersonnes';
import CreationCompteAdmin from '../Pages/CreationCompteAdmin';
import ChoixUser from '../Pages/ChoixUser';
import ModifierUser from '../Pages/ModifierUser';
import BoiteReception from '../Pages/BoiteReception';
import MessagesEnvoyes from '../Pages/MessageEnvoyes';
import MessagesArchives from '../Pages/MessagesArchives';
import VoirMessage from '../Pages/VoirMessage';
import NouveauMessage from '../Pages/NouveauMessage';
import MenuUser from '../Pages/MenuUser';
import ModifierReserv from '../Pages/ModifierReserv';
import PrivateRouteAdmin from './PrivateRouteAdmin';
import { ToastProvider } from 'react-toast-notifications';
import Deconnexion from '../Composants/Deconnexion';
import AjoutMateriel from '../Pages/AjoutMateriel';
import AjoutRole from '../Pages/AjoutRole';

export default function Chemin() {
    return (
        <Router>
            <div>
                {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
                <ToastProvider>
                    <Switch>
                        <Route path="/deconnexion">
                            <Deconnexion/>
                        </Route>
                        <PrivateRouteAdmin path="/modifierReservation">
                            <ModifierReserv />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/nouveauMessage">
                            <NouveauMessage />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/voirMessage">
                            <VoirMessage />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/messagesArchives">
                            <MessagesArchives />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/messagesEnvoyes">
                            <MessagesEnvoyes />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/boiteReception">
                            <BoiteReception />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/modifierUser">
                            <ModifierUser />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/choixUser">
                            <ChoixUser />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/creationCompteAdmin">
                            <CreationCompteAdmin />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/listePersonnes">
                            <ListePersonnes />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/reserverSalle">
                            <ReserverSalle />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/modifierSalle">
                            <ModifierSalle />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/choixSalle">
                            <ChoixSalle />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/salleComplete">
                            <SalleComplete />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/listeSalles">
                            <ListeSalles />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/creationSalle">
                            <CreationSalle />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/menuAdmin">
                            <MenuAdmin />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/menuUser">
                            <MenuUser />
                        </PrivateRouteAdmin>
                        <Route path="/creationCompte">
                            <CreationCompte />
                        </Route>
                        <PrivateRouteAdmin path="/ajoutMateriel">
                            <AjoutMateriel />
                        </PrivateRouteAdmin>
                        <PrivateRouteAdmin path="/ajoutRole">
                            <AjoutRole />
                        </PrivateRouteAdmin>
                        <Route path="/">
                            <Authentification />
                        </Route>
                    </Switch>
                </ToastProvider>
            </div>
        </Router>
    );
}