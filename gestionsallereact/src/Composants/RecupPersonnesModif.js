import React, { useState, useEffect, Fragment } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { CODE_ADMIN, CODE_NOAUTH } from '../CodeRole';

export default function RecupPersonnesModif(props) {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState()

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, props.filtre, setCodeRole);
            setPret(true);
        }
        fetchJson();
        recup(setListe, props.filtre, setCodeRole);
    }, []);

    return <Fragment>
        {(pret && codeRole != 200) ? (<Redirect to="" />) :
        (pret ? (
            <div class="Container" id="marge_haut_bis">
                <div class="row col-md-12 col-md-offset-2 ">
                    <table className=" table table-striped fond">
                        <thead class="thead-dark">
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Date de naissance</th>
                                <th>Actif</th>
                                <th>Mail</th>
                                <th>Adresse</th>
                                <th>Rôle</th>
                                <th></th>
                            </tr>
                        </thead>
                        {recupListe(liste)}
                    </table>
                </div>
            </div>
        ) : (<div></div>))}
    </Fragment>
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((personne) =>
                <tr key={personne.id_personne}>
                    <td>{personne.nom}</td>
                    <td>{personne.prenom}</td>
                    <td>{personne.dateNaissance.dayOfMonth + "/" + personne.dateNaissance.monthValue + "/" + personne.dateNaissance.year}</td>
                    <td>{"" + personne.actif}</td>
                    <td>{personne.email}</td>
                    <td>{personne.adresse}</td>
                    <td>{personne.role.role}</td>
                    <td>
                        <Link to={`/modifierUser?idUser=${personne.id_personne}`} >
                            <input className="btn btn-warning btn-block" type="button" value="Modifier" />
                        </Link>
                    </td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe, bool, setCodeRole) {
    if (bool === undefined) {
        await fetch(`${process.env.REACT_APP_API_URL}/SVURest`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => { setListe(json.objetRetourne);
                setCodeRole(json.codeRole); })
            .catch(error => { console.log(error); })
    }
    else {
        await fetch(`${process.env.REACT_APP_API_URL}/SVUUserRest`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => { setListe(json.objetRetourne);
                setCodeRole(json.codeRole); })
            .catch(error => { console.log(error); })
    }
}
