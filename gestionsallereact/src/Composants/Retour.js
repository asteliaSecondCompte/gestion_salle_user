import React from 'react';
import { Link } from 'react-router-dom';

export default function Retour() {
    return (
        <footer>
            <ul className="nav nav-tabs">
                <li className="nav-item">
                    <Link className="nav-link deconnexion" to="/menuAdmin">Retour</Link>
                </li>
            </ul>
        </footer>
    );
}
