import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { CODE_ADMIN, CODE_NOAUTH } from '../CodeRole';
import {Redirect} from 'react-router-dom';

export default function RecupBatiment(props) {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState()

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, setCodeRole, props.setBatiment);
            setPret(true);
        }
        fetchJson();
        recup(setListe, setCodeRole, props.setBatiment);
    }, []);

    const val = () => {
        if (props.valeur !== undefined ) {
            return <option value={props.valeur}>{props.valeur}</option>
        }
        return;
    }
    
    if (pret && codeRole != 200) {
        return (<Redirect to="" />);
    }
    else if (pret) {
        return (
            <Form.Control id="batiment" as="select" onChange={props.handleBatiment}>
                {val()}
                {recupListe(liste)}
            </Form.Control>
        )
    }
    else {
        return (
            <Form.Control id="batiment" as="select" onChange={props.handleBatiment}>
            </Form.Control>
        )
    }
    
}

function recupListe(liste) {
    return (
        liste.map((listebatiment) =>
        <option id={`${listebatiment.id}`} value={`${listebatiment.id}`} key={`${listebatiment.id}`}>{ listebatiment.nom} </option>
        )
    );
}

async function recup(setListe, setCodeRole, setBatiment) {
    await fetch(`${process.env.REACT_APP_API_URL}/crbRest`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => response.json())
        .then(json => { setListe(json.objetRetourne);
            setCodeRole(json.codeRole);
            setBatiment(json.objetRetourne[0].id)
        })
        .catch(error => { console.log(error); })
}