import React from 'react';
import Message from './Message';

export default function RecupMessages(props) {

    if (props.value === "recu") {
        return recus(props);
    }
    else if (props.value === "envoye") {
        return envoyes(props);
    }
    else {
        return archives(props);
    }

}

function archives(props) {
    return (
        <ul>
            <Message value="false" 
                     expediteur="joker"
                     destinataire="Moi"
                     objet="Chauve pourrie"
                     message="Souris un peu plus"
                     date="02/02/2020"
                     idMessage="1" />
        </ul>
    );
}

function envoyes(props) {
    return (
        <ul >
            <Message value="true" 
                     personne="nightwing"
                     objet="Re : Planque du Pingouin"
                     message="Je suis juste derrière toi"
                     date="02/02/2020"
                     idMessage="3"
                     page="/messagesEnvoyes"
                      />
        </ul>
    );
}

function recus(props) {
    return (
        <ul class="text-center">
            <Message value="true" 
                     personne="nightwing"
                     objet="Planque du Pingouin"
                     message="J'ai trouvé l'une des..."
                     date="02/02/2020"
                     idMessage="2"
                     page="/boiteReception" 
                     />
        </ul>
    );
}