import React, { useState, useEffect, Fragment } from 'react';
import { Container } from 'react-bootstrap';
import {Redirect} from 'react-router-dom';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function RecupReservs(props) {

    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState();

    const getReservs = () => {
        fetch(`${process.env.REACT_APP_API_URL}/ListeReservsRest?idSalle=${ props.idSalle }`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => {setListe(json.objetRetourne)
                setCodeRole(json.codeRole)
            })
            .catch(error => console.log(error));
    }

    const recupListe = (liste) => {
        return (
            <tbody>
                {liste.map((reserv) =>
                    <tr key={reserv.id}>
                        <td>{reserv.id}</td>
                        <td>{reserv.intitule}</td>
                        <td>{reserv.dateDebut.dayOfMonth + "/" + reserv.dateDebut.monthValue + "/" + reserv.dateDebut.year}</td>
                        <td>{reserv.dateFin.dayOfMonth + "/" + reserv.dateFin.monthValue + "/" + reserv.dateFin.year}</td>
                    </tr>
                )}
            </tbody>
        );
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getReservs();
            setPret(true);
        }
        fetchJson();
        getReservs();
    }, []);

    return <Fragment>
        {(pret && codeRole == 202) ? (<Redirect to=""/>) : 
        (pret ? (
            <Container>
                <table className="tableCentree enteteTableau fond">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Intitule</th>
                            <th>Date de début</th>
                            <th>Date de fin</th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
            </Container>
        ) : (<div></div>))}
    </Fragment>

}