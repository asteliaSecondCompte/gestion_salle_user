import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';

export default function Menu() {
    return (
        <header>
            <Navbar collapseOnSelect expand="lg" className="nav nav-tabs">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse>
                    <Nav className="mr-auto">
                        <NavDropdown title="Gestion Utilisateurs" id="collasible-nav-dropdown" style={{color: "blue"}}>
                            <NavDropdown.Item href="/listePersonnes">Lister les personnes</NavDropdown.Item>
                            <NavDropdown.Item href="/creationCompteAdmin">Créer un compte</NavDropdown.Item>
                            <NavDropdown.Item href="/choixUser">Modifier un compte</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Gestion Salles" id="collasible-nav-dropdown">
                            <NavDropdown.Item href="/listeSalles">Lister les salles</NavDropdown.Item>
                            <NavDropdown.Item href="/creationSalle">Créer une salle</NavDropdown.Item>
                            <NavDropdown.Item href="/choixSalle">Modifier une salle</NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                    <Nav>
                        <Nav.Link href="/deconnexion">Déconnexion</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </header>
    );
}
