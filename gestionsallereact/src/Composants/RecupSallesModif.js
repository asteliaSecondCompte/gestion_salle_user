import React, { useState, useEffect, Fragment } from 'react';
import { Link, Redirect } from "react-router-dom";
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function RecupSalles() {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState(202);

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, setCodeRole);
            setPret(true);
        }
        fetchJson();
        recup(setListe, setCodeRole);
    }, []);


    return <Fragment>
    
        {(pret && codeRole != 200) ? (<Redirect to=""/>) :
        (pret ? (
            <div class="Container">
            <div class="row col-md-12 col-md-offset-2 ">
                <table class="table table-striped fond">
                    <thead class="thead-dark">
                        <tr>
                            <th>Numéro</th>
                            <th>Type de salle</th>
                            <th>Nom</th>
                            <th>Capacité</th>
                            <th>Surface</th>
                            <th>Bâtiment</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
           </div>
           </div>
        ) : (<div></div>))}

    </Fragment >
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((salle) =>
                <tr key={salle.id_salle}>
                    <td>{salle.numero}</td>
                    <td>{salle.typeSalle}</td>
                    <td>{salle.nom}</td>
                    <td>{salle.capacite}</td>
                    <td>{salle.surface}</td>
                    <td>{salle.batiment.nom}</td>
                        
                    <td>
                        <Link to={`/salleComplete?idSalle=${salle.id}`}>
                            <input className="btn btn-info btn-block " type="submit" value="Voir" />
                        </Link>
                        </td>
                        <td>
                        <Link to={`/modifierSalle?idSalle=${salle.id}`} >
                            <input className="btn btn-warning btn-block" type="button" value="Modifier" />
                        </Link>
                        </td>
                        <td>
                        <Link to={`/reserverSalle?idSalle=${salle.id}`}>
                            <input className="btn btn-success btn-block" type="button" value="Réserver" />
                        </Link>
                    </td>
                      
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe, setCodeRole) {
    await fetch(`${process.env.REACT_APP_API_URL}/vsRest`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => response.json())
        .then(json => { setListe(json.objetRetourne)
            setCodeRole(json.codeRole) })
        .catch(error => { console.log(error); })
}