import React, { useState, useEffect } from 'react';
import { Form } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

export default function RecupTypeSalle(props) {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState();

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, setCodeRole);
            setPret(true);
        }
        fetchJson();
        recup(setListe, setCodeRole);
    }, []);

    const val = () => {
        if (props.valeur !== undefined ) {
            return <option id={props.valeur} value={props.valeur}>{props.valeur}</option>
        }
        return;
    }
    if (pret && codeRole != 200) {
        return (<Redirect to="" />);
    }
    else if (pret) {
        return (
            <Form.Control id="type" as="select" onChange={props.handleType}>
                { val() }
                {recupListe(liste)}
            </Form.Control>
        )
    }
    else {
        return (
            <Form.Control id="batiment" as="select" onChange={props.handleBatiment}>
            </Form.Control>
        )
    }
}

function recupListe(liste) {
    let i = 1;
    return (
        liste.map((listeTypeSalle) =>
        <option id={listeTypeSalle} value={`${++i}`} key={ i }>{ listeTypeSalle} </option>
        )
    );
}

async function recup(setListe, setCodeRole) {
    await fetch(`${process.env.REACT_APP_API_URL}/rtsRest`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => response.json())
        .then(json => { 
            setListe(json.objetRetourne)
            setCodeRole(json.codeRole)
        })
        .catch(error => { console.log(error); })
}