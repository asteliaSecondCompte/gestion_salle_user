import React from 'react';
import { Container } from 'react-bootstrap';
import { FaCheckCircle } from 'react-icons/fa';

export default function Reussite(props) {

    return (
        <Container>
            <h2>{props.texte}</h2>
            <FaCheckCircle color="green" size="100" />
        </Container>
    );

} 