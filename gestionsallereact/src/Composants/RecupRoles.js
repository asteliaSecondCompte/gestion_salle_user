import React, { useState, Fragment, useEffect } from 'react';

var nb = 1;

export default function RecupRoles(props) {

    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState(-1);

    const getRoles = () => {
        fetch(`${process.env.REACT_APP_API_URL}/ListeRolesRest`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
        .then(response => response.json())
        .then(json => {
            setListe(json.objetRetourne);
            setCodeRole(json.codeRole);
            if (json.objetRetourne.length > 0) {
                props.setRole(json.objetRetourne[0]);
            }
        })
        .catch(error => console.log(error));
        
        
    }
    
    const val = () => {
        if (props.valeur !== undefined ) {
            console.log(props)
            return <option key={ nb++ } >{props.valeur.role}</option>
        }
        return;
    }
    
    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getRoles();
            setPret(true);
        }
        fetchJson();
        getRoles();
    }, []);
    console.log("liste =")
    console.log(liste)
    return <Fragment>
        {pret ? (
            <select id={ props.id } className="form-control" onChange={props.handleRole}>
                    { val() }
                    {liste.map((role) => <option key={nb++}>{role.role}</option>)}
                </select>
        ) : (<option></option>)}
    </Fragment>

}