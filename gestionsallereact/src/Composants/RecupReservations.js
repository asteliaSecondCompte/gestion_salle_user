import React, { useState, useEffect, Fragment } from 'react';
import { Container, ListGroup, Table } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import {CODE_NOAUTH, CODE_ADMIN} from '../CodeRole';

export default function RecupReservations(props) {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState();

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, setCodeRole);
            setPret(true);
        }
        fetchJson();
        recup(setListe, setCodeRole);
    }, []);

    return <Fragment>

        {(pret && codeRole == 202) ? (<Redirect to=""/>) :
        (pret ? (
            <div class="Container">
            <div class="row col-md-12 col-md-offset-2 ">
                <table class="table table-striped fond">
                    <thead class="thead-dark">
                        <tr>
                            <th>Id</th>
                            <th>Intitule</th>
                            <th>DateDebut</th>
                            <th>DateFin</th>
                            <th></th>
                                       
                        </tr>
                    </thead>
                    {recupListe(liste, props.idSalle)}
                </table>
            </div>
</div>
        ) : (<div></div>))}
   
    </Fragment>
}

function recupListe(liste, idSalle) {
    return <Fragment>
    
    

        <tbody>
   
            {liste.map((reservation) =>
                <tr key={reservation.id}>
                    <td>{reservation.id}</td>
                    <td>{reservation.intitule}</td>
                    <td>{reservation.dateDebut.dayOfMonth + "/" + reservation.dateDebut.monthValue + "/" + reservation.dateDebut.year}</td>
                    <td>{reservation.dateFin.dayOfMonth + "/" + reservation.dateFin.monthValue + "/" + reservation.dateFin.year}</td>
                    <td>
                    
                    <Link to={"modifierReservation?idSalle="+idSalle+"&&idReserv=" + reservation.id} >
                                  <input className="btn btn-warning" type="button" value="Modifier" />
                     </Link>
                    
                    </td>
                </tr>
            )}
        </tbody>
           
    </Fragment>
    
}

async function recup(setListe, setCodeRole) {
    let idSalle = parseInt(document.URL.split("=")[1]);
    await fetch(`${ process.env.REACT_APP_API_URL }/ListeReservsRest?idSalle=${idSalle}`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
        .then(response => response.json())
        .then(json => { setListe(json.objetRetourne)
            setCodeRole(json.codeRole)
        })
        .catch(error => { console.log(error); })
}

