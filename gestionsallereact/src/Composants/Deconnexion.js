import React from 'react';
import { Redirect } from 'react-router-dom';

export default function Deconnexion() {
    
    sessionStorage.clear();
    
    return (
        <Redirect to="/"/>
    );
}