import React, { useState, Fragment, useEffect } from 'react';

var nb = 1;

export default function RecupMateriels() {

    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState()
    let id = 101;

    const getMateriels = () => {
        fetch(`${process.env.REACT_APP_API_URL}/ListeMaterielsRest?idSalle=${id}`, {
            method : "get",
            headers : {
                "Authorization": sessionStorage.token
            }
        })
            .then(response => response.json())
            .then(json => {
                setListe(json.objetRetourne);
                setCodeRole(json.codeRole);
            })
            .catch(error => console.log(error));
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getMateriels();
            setPret(true);
        }
        fetchJson();
        getMateriels();
    }, []);

    return <Fragment>
        {(pret && codeRole == 202) ? <Redirect to=""/> :
        (pret ? (<tbody>
            {liste.map((materiel) => <tr><td>{materiel}</td>
                                    <td>{materiel.quantite}</td></tr>)}
            </tbody>
        ) : (<tbody></tbody>))}
    </Fragment>

}