//import React, { useState } from 'react';

export default function ModifSuppReser() {

    const [status, setStatus] = useState(false);
    let idReservation = document.URL.split("=")[1];


    const modifierReserv = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/ModifierRest`, {
            method : "put",
            headers: {
                "Content-type": "application/json; charset=UTF-8",
                Accept: 'application/json'
            },
            body : `{
                        "idReservation" : ${ idReservation },
                        "intitule" : "${ intitule }",
                        "dateDebut" : "${ dateDebut }",
                        "duree : "${ duree }",
                        "idSalle" : "${ idSalle }"
                    }`
        })
            .then(response => response.json())
            .then(json => setStatus(json))
            .catch(error => {
                setStatus(false);
                console.log(error);
            })
    }

    const annulerReserv = () => {
        fetch(`${ process.env.REACT_APP_API_URL }/AnnulerRest?idReservation=${ idReservation }`, {
            method : "delete"
        })
            .then(response => response.json())
            .then(json => setStatus(json))
            .catch(error => {
                seetStatus(false);
                console.log(error);
            })
    }

   
}